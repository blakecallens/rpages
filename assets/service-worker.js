/* eslint-disable */

const CACHE_NAME = 'rpages-cache-v1';
const URLS_TO_CACHE = [
	'/offline',
	'/assets/fonts/roboto-regular.woff',
	'/assets/fonts/roboto-medium.woff',
	'/assets/fonts/roboto-bold.woff',
];

self.addEventListener('install', event => {
	event.waitUntil(caches.open(CACHE_NAME).then(cache => cache.addAll(URLS_TO_CACHE)));
});

self.addEventListener('fetch', event => {
	if (event.request.mode === 'navigate') {
		event.respondWith(fetch(event.request).catch(() => caches.open(CACHE_NAME).then(cache => cache.match('/offline'))));
	}
});

self.addEventListener('activate', event => {
	event.waitUntil(caches.keys().then(keyList =>
		Promise.all(keyList.map(key => {
			if (key !== CACHE_NAME) {
				return caches.delete(key);
			}
		}))
	));
});