module.exports = {
  extends: 'airbnb',
  'plugins': [
    'jsdoc'
  ],
  rules: {
    'arrow-parens': 0,
    'import/newline-after-import': 0,
    'class-methods-use-this': 0,
    'no-await-in-loop': 0,
    'no-underscore-dangle': 0,
    'func-names': 0,
    'space-before-function-paren': ['error', 'always'],
    'indent': ['error', 'tab', { 'SwitchCase': 1 }],
    'no-tabs': 0,

    'jsdoc/check-examples': 1,
    'jsdoc/check-param-names': 1,
    'jsdoc/check-tag-names': 1,
    'jsdoc/check-types': 1,
    'jsdoc/no-undefined-types': 1,
    'jsdoc/require-param': 1,
    'jsdoc/require-param-description': 1,
    'jsdoc/require-param-name': 1,
    'jsdoc/require-param-type': 1,
    'jsdoc/require-returns-check': 1,
    'jsdoc/require-returns-description': 1,
    'jsdoc/require-returns-type': 1,
    'jsdoc/valid-types': 1
  },
}