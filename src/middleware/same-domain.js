const pino = require('pino')({
	name: 'SameDomainMiddleware',
	prettyPrint: true,
});
const consts = require('../consts');

/**
 * Forces all requests to come from the same domain
 *
 * @param {object} req The Express request object
 * @param {object} res The Express response object
 * @param {Function} next The Express next function
 */
module.exports = (req, res, next) => {
	if (consts.HOST.indexOf('localhost') === -1 && req.get('host') !== consts.HOST) {
		pino.warn(`Unauthorized request from ${req.get('host')}. Expected host is ${consts.HOST}`);
		res.sendStatus(403);
		return;
	}
	next();
};
