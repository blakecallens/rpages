const _ = require('lodash');
const consts = require('../consts');
const cookieService = require('../services/cookie');
const redisService = require('../services/redis');
const dataUtil = require('../data/util');
const subData = require('../data/sub');
const userData = require('../data/user');

const noUserExit = (req, res, next) => {
	cookieService.delete(res, `${consts.SESSION_PREFIX}${req.cookies.sessionId}`);
	next();
};

/**
 * Checks for an active session and populates user if it exists
 *
 * @param {object} req The Express request object
 * @param {object} res The Express response object
 * @param {Function} next The Express next function
 */
module.exports = async (req, res, next) => {
	try {
		if (_.get(req, ['cookies', consts.SESSION_COOKIE_NAME])) {
			const _id = await redisService.get(`session_${req.cookies.sessionId}`);
			if (_.isEmpty(_id)) {
				noUserExit(req, res, next);
				return;
			}

			const user = await userData.getUser(_id);
			if (_.isEmpty(user)) {
				noUserExit(req, res, next);
				return;
			}

			_.set(req, 'user', _.assignIn(user.clean(), {
				memberSince: dataUtil.getShortDate(user.created),
				subscribers: await subData.getUserSubCount(user._id),
			}));
		}
		next();
	} catch (err) {
		next();
	}
};
