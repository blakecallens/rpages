const multer = require('multer');
const consts = require('../consts');

const upload = multer({ limits: { fileSize: consts.MAX_FILE_SIZE } }).single('image');


/**
 * Uses multer to setup file data
 * @param {object} req The Express request object
 * @param {object} res The Express response object
 * @param {Function} next The Express next function
 */
module.exports = (req, res, next) => {
	upload(req, res, err => {
		if (err) {
			req.fileError = err;
		}
		next();
	});
};
