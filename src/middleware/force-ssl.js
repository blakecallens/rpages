/**
 * If the FORCE_SSL env variable is set, will redirect to https
 * @param {object} req The Express request object
 * @param {object} res The Express response object
 * @param {Function} next The Express next function
 */
module.exports = (req, res, next) => {
	if (process.env.FORCE_SSL && req.header('x-forwarded-proto') !== 'https') {
		res.redirect(`https://${req.header('host')}${req.url}`);
	} else {
		next();
	}
};
