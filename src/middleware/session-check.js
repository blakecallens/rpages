const _ = require('lodash');
const consts = require('../consts');

/**
 * Checks for an active session and redirects to dashboard if one exists
 *
 * @param {object} req The Express request object
 * @param {object} res The Express response object
 * @param {Function} next The Express next function
 */
module.exports = (req, res, next) => {
	if (_.get(req, ['cookies', consts.SESSION_COOKIE_NAME])) {
		res.redirect('/dashboard');
		return;
	}
	next();
};
