const _ = require('lodash');
const consts = require('../consts');
const cookieService = require('../services/cookie');
const redisService = require('../services/redis');
const dataUtil = require('../data/util');
const userData = require('../data/user');
const subData = require('../data/sub');

/**
 * Checks for an active session and populates user if it exists
 *
 * @param {object} req The Express request object
 * @param {object} res The Express response object
 * @param {Function} next The Express next function
 */
module.exports = async (req, res, next) => {
	try {
		if (_.get(req, ['cookies', consts.SESSION_COOKIE_NAME])) {
			const _id = await redisService.get(`${consts.SESSION_PREFIX}${req.cookies.sessionId}`);
			if (_.isEmpty(_id)) {
				cookieService.delete(res, `${consts.SESSION_PREFIX}${req.cookies.sessionId}`);
				throw new Error();
			}

			const user = await userData.getUser(_id);

			if (_.isEmpty(user)) {
				cookieService.delete(res, `${consts.SESSION_PREFIX}${req.cookies.sessionId}`);
				throw new Error();
			}

			_.set(req, 'user', _.assignIn(user.clean(['imgur']), {
				memberSince: dataUtil.getShortDate(user.created),
				subscribers: await subData.getUserSubCount(_id),
			}));
			next();
		} else {
			throw new Error();
		}
	} catch (err) {
		res.redirect('/signin');
	}
};
