/**
 * If the FORWARD_TO env variable is set, will redirect to that domain
 * @param {object} req The Express request object
 * @param {object} res The Express response object
 * @param {Function} next The Express next function
 */
module.exports = (req, res, next) => {
	if (process.env.FORWARD_TO) {
		res.redirect(`https://${process.env.FORWARD_TO}${req.url}`);
	} else {
		next();
	}
};
