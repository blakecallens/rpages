/**
 * Cleans up response headers to remove things added by other platforms
 * @param {object} req The Express request object
 * @param {object} res The Express response object
 * @param {Function} next The Express next function
 */
module.exports = (req, res, next) => {
	res.setHeader('X-Powered-By', 'Collected Times');
	next();
};
