const _ = require('lodash');
const { ObjectId } = require('mongodb');
const mongoose = require('mongoose');
const pino = require('pino')({
	name: 'SubData',
	prettyPrint: true,
});
const dataUtil = require('./util');
const subData = require('./sub');
const userSchema = require('../schemas/user');
const consts = require('../consts');

class UserData {
	constructor () {
		this.User = mongoose.model('User', userSchema);
	}

	/**
	 * Retrieves a user by _id or handle
	 *
	 * @param {object|string} _id The _id or handle of the user
	 * @returns {object} The user
	 */
	async getUser (_id) {
		try {
			let user;
			if (ObjectId.isValid(_id)) {
				user = await this.User.findOne({ _id });
			}
			if (_.isEmpty(user)) {
				user = await this.User.findOne({ handle: _id.toLowerCase() });
			}
			return user;
		} catch (err) {
			pino.error(`Unable to retrieve user ${_id}`);
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Retrieves a user by email
	 *
	 * @param {string} email The email of the user
	 * @returns {object} The user
	 */
	async getUserByEmail (email) {
		try {
			return await this.User.findOne({ email });
		} catch (err) {
			pino.error('Unable to retrieve user by email');
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Retrieves a page of users from the DB
	 *
	 * @param {object} req The express request object
	 * @param {object} query The DB query
	 * @param {number} page The page number
	 * @param {object} options Retrieval options
	 * @returns {object[]} The users
	 */
	async getUsers (req, query, page, options = {}) {
		try {
			const opts = _.assignIn({
				sort: { created: -1 },
			}, options);

			const result = await this.User.paginate(query, {
				offset: page * consts.PAGE_LIMIT,
				limit: consts.PAGE_LIMIT,
				sort: opts.sort,
				lean: true,
			});
			await Promise.all(_.map(result.docs, async doc => _.assignIn(doc, {
				notLoggedInUser: !doc._id.equals(_.get(req, 'user._id')),
				subscribed: _.get(req, 'user') ? await subData.userIsSubscribed(req.user._id, doc._id) : undefined,
			})));
			return _.assignIn(result, dataUtil.getPagination(result, page));
		} catch (err) {
			pino.error('Unable to retrieve users');
			pino.error(err);
			throw err;
		}
	}
}

module.exports = new UserData();
