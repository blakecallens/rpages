const _ = require('lodash');
const { DateTime } = require('luxon');
const mongoose = require('mongoose');
const pino = require('pino')({
	name: 'PostData',
	prettyPrint: true,
});
const dataUtil = require('./util');
const subData = require('./sub');
const postSchema = require('../schemas/post');
const userSchema = require('../schemas/user');
const postService = require('../services/post');
const consts = require('../consts');

class PostData {
	constructor () {
		this.Post = mongoose.model('Post', postSchema);
		this.User = mongoose.model('User', userSchema);
	}

	/**
	 * Retrieves a post from the DB
	 *
	 * @param {object} req The express request object
	 * @param {object|string} _id The _id of the post
	 * @param {object} options Retrieval options
	 * @returns {object} The post
	 */
	async getPost (req, _id, options = {}) {
		try {
			const opts = _.assignIn({
				getReferences: false,
			}, options);

			let post = await this.Post.findOne({ _id }).populate('user');
			post.user = post.user.clean(['customCss', 'spotifyUrl']);

			let references;
			if (opts.getReferences && post.user._id.equals(_.get(req, 'user._id'))) {
				const refQuery = { references: { $regex: `.*${_id}.*` } };
				if (_.get(req, 'user.hideNonSubMentions')) {
					const subs = await subData.getAllUserSubscriptions(req.user._id);
					_.set(refQuery, 'user', { $in: _.map(subs, 'subscribed') });
				}
				references = await this.Post.countDocuments(refQuery);
			}

			post = post.toObject();
			if (!_.isEmpty(post.user.spotifyUrl)) {
				if (post.user.spotifyUrl.match(/soundcloud\.com/)) {
					post.user.soundCloud = true;
				} else {
					post.user.spotifyUrl = post.user.spotifyUrl.replace(/https:\/\/open\.spotify\.com\//, '');
				}
			}

			if (!_.isEmpty(post)) {
				_.assignIn(post, {
					references,
					source: post.content.replace(/\n/g, '<br/>'),
					content: await postService.replaceUrls(req, post.content),
					createdISO: post.created.toISOString(),
					readableDate: dataUtil.getReadableDate(post.created),
					canDelete: post.user._id.equals(_.get(req, 'user._id')),
					canPin: post.user._id.equals(_.get(req, 'user._id')) && !post.pinned,
				});
				if (post.canDelete && opts.getReferences) {
					post.refereces = await this.Post.countDocuments({ references: { $regex: `.*${post._id}.*` } });
				}
			}
			return post;
		} catch (err) {
			pino.error(`Unable to retrieve post ${_id}`);
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Retrieves a page of posts from the DB
	 *
	 * @param {object} req The express request object
	 * @param {object} query The DB query
	 * @param {number} page The page number
	 * @param {object} options Retrieval options
	 * @returns {object[]} The posts
	 */
	async getPosts (req, query, page, options = {}) {
		try {
			const opts = _.assignIn({
				getReferences: false,
				sort: { created: -1 },
			}, options);

			const result = await this.Post.paginate(query, {
				offset: page * consts.PAGE_LIMIT,
				limit: consts.PAGE_LIMIT,
				sort: opts.sort,
				lean: true,
				populate: 'user',
			});

			const subsQuery = {};
			if (opts.getReferences && _.get(req, 'user.hideNonSubMentions')) {
				const subs = await subData.getAllUserSubscriptions(req.user._id);
				_.set(subsQuery, 'user', { $in: _.map(subs, 'subscribed').concat(req.user._id) });
			}

			result.docs = _.map(result.docs, doc => _.omit(doc, 'user.password'));
			await Promise.all(_.map(result.docs, async doc => _.assignIn(doc, {
				references: opts.getReferences && doc.user._id.equals(_.get(req, 'user._id'))
					? await this.Post.countDocuments(_.assignIn({ references: { $regex: `.*${doc._id}.*` } }, subsQuery)) : undefined,
				youReferenced: _.get(req, 'user')
					? await this.Post.countDocuments({ user: req.user._id, references: { $regex: `.*${doc._id}.*` } }) : undefined,
				source: doc.content.replace(/\n/g, '<br/>'),
				content: await postService.replaceUrls(req, doc.content),
				createdISO: doc.created.toISOString(),
				readableDate: dataUtil.getReadableDate(doc.created),
				canDelete: doc.user._id.equals(_.get(req, 'user._id')),
				canPin: doc.user._id.equals(_.get(req, 'user._id')) && !doc.pinned,
			})));
			return _.assignIn(result, dataUtil.getPagination(result, page));
		} catch (err) {
			pino.error('Unable to retrieve posts');
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Retrieves a page of post references from the DB
	 *
	 * @param {object} req The express request object
	 * @param {object|string} _id The _id of the post
	 * @param {number} page The page number
	 * @param {object} options Retrieval options
	 * @returns {object[]} The references
	 */
	async getReferences (req, _id, page, options = {}) {
		try {
			const opts = _.assignIn({
				sort: { created: -1 },
			}, options);

			const query = { references: { $regex: `.*${_id}.*` } };
			if (_.get(req, 'user.hideNonSubMentions')) {
				const subs = await subData.getAllUserSubscriptions(req.user._id);
				_.set(query, 'user', { $in: _.map(subs, 'subscribed').concat(req.user._id) });
			}

			const result = await this.Post.paginate(query, {
				offset: page * consts.PAGE_LIMIT,
				limit: consts.PAGE_LIMIT,
				sort: opts.sort,
				lean: true,
				populate: 'user',
			});

			await Promise.all(_.map(result.docs, async doc => _.assignIn(doc, {
				source: doc.content.replace(/\n/g, '<br/>'),
				content: await postService.replaceUrls(req, doc.content, true),
				createdISO: doc.created.toISOString(),
				readableDate: dataUtil.getReadableDate(doc.created),
				canDelete: doc.user._id.equals(req.user._id),
			})));
			return _.assignIn(result, dataUtil.getPagination(result, page));
		} catch (err) {
			pino.error(`Unable to retrieve references for post ${_id}`);
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Returns the number of mentions of a handle, in the last day, by a subset of users
	 *
	 * @param {string} handle The user handle to search for
	 * @param {object} [userQuery] A user query to filter posts by. Defaults to all users.
	 * @returns {number} The number of mentions
	 */
	async getMentionCount (handle, userQuery) {
		try {
			const query = {
				mentions: `@${handle}`,
				created: { $gt: DateTime.local().minus({ days: 1 }).toJSDate() },
			};
			if (!_.isEmpty(userQuery)) {
				query.user = userQuery;
			}
			return this.Post.countDocuments(query);
		} catch (err) {
			pino.error(`Unable to retrieve sub mention count for user ${handle}`);
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Returns the top 10 trending hashtags
	 *
	 * @param {object} req The express request object
	 */
	async getTrendingHashtags (req) {
		try {
			const trending = await this.Post.aggregate([
				{
					$match: {
						created: { $gt: DateTime.local().minus({ days: 1 }).toJSDate() },
						hashtags: { $gt: [] },
					},
				},
				{
					$project: {
						_id: 0,
						user: 1,
						hashtags: 1,
					},
				},
				{ $unwind: '$hashtags' },
				{
					$group: {
						_id: {
							user: '$user',
							hashtag: { $toLower: '$hashtags' },
						},
					},
				},
				{ $sortByCount: '$_id.hashtag' },
				{ $limit: 10 },
			]);

			return await Promise.all(_.map(trending, async item => {
				const result = await this.Post.paginate({ hashtags: item._id }, {
					offset: 0,
					limit: 1,
					sort: { created: -1 },
					populate: 'user',
				});
				let post = _.head(result.docs);
				post.user = post.user.clean();
				post = post.toObject();
				_.assignIn(post, {
					references: post.user._id.equals(_.get(req, 'user._id'))
						? await this.Post.countDocuments(_.assignIn({ references: { $regex: `.*${post._id}.*` } }, {})) : undefined,
					youReferenced: _.get(req, 'user')
						? await this.Post.countDocuments({ user: req.user._id, references: { $regex: `.*${post._id}.*` } }) : undefined,
					source: post.content.replace(/\n/g, '<br/>'),
					content: await postService.replaceUrls(req, post.content),
					createdISO: post.created.toISOString(),
					readableDate: dataUtil.getReadableDate(post.created),
					canDelete: post.user._id.equals(_.get(req, 'user._id')),
					canPin: post.user._id.equals(_.get(req, 'user._id')) && !post.pinned,
				});

				return {
					hashtag: item._id.substr(1),
					count: item.count,
					posts: [post],
				};
			}));
		} catch (err) {
			pino.error('Unable to retrieve trending hashtags');
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Retrieves the top 10 active users for the day
	 */
	async getActiveUsers () {
		try {
			const posts = await this.Post.aggregate([
				{
					$match: {
						created: { $gt: DateTime.local().minus({ days: 1 }).toJSDate() },
					},
				},
				{ $sortByCount: '$user' },
				{ $limit: 10 },
			]);
			const users = await this.User.find({ _id: { $in: _.map(posts, '_id') } });
			return _.map(posts, post => {
				const user = _.find(users, { _id: post._id });
				return _.assignIn(user.clean(), { count: post.count });
			});
		} catch (err) {
			pino.error('Unable to retrieve active users');
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Returns the top 10 trending users
	 *
	 * @param {object} req The express request object
	 */
	async getTrendingUsers (req) {
		try {
			const trending = await this.Post.aggregate([
				{
					$match: {
						created: { $gt: DateTime.local().minus({ days: 1 }).toJSDate() },
						mentions: { $gt: [] },
					},
				},
				{
					$project: {
						_id: 0,
						user: 1,
						mentions: 1,
					},
				},
				{ $unwind: '$mentions' },
				{
					$group: {
						_id: {
							user: '$user',
							mention: { $toLower: '$mentions' },
						},
					},
				},
				{ $sortByCount: '$_id.mention' },
				{ $limit: 10 },
			]);

			return _.compact(await Promise.all(_.map(trending, async item => {
				const [user, result] = await Promise.all([
					this.User.findOne({ handle: item._id.substr(1) }),
					this.Post.paginate({ mentions: item._id }, {
						offset: 0,
						limit: 1,
						sort: { created: -1 },
						populate: 'user',
					}),
				]);
				if (!user) {
					return undefined;
				}

				let post = _.head(result.docs);
				post.user = post.user.clean();
				post = post.toObject();
				_.assignIn(post, {
					references: post.user._id.equals(_.get(req, 'user._id'))
						? await this.Post.countDocuments(_.assignIn({ references: { $regex: `.*${post._id}.*` } }, {})) : undefined,
					youReferenced: _.get(req, 'user')
						? await this.Post.countDocuments({ user: req.user._id, references: { $regex: `.*${post._id}.*` } }) : undefined,
					source: post.content.replace(/\n/g, '<br/>'),
					content: await postService.replaceUrls(req, post.content),
					createdISO: post.created.toISOString(),
					readableDate: dataUtil.getReadableDate(post.created),
					canDelete: post.user._id.equals(_.get(req, 'user._id')),
					canPin: post.user._id.equals(_.get(req, 'user._id')) && !post.pinned,
				});

				return {
					mention: item._id.substr(1),
					user: user.clean(),
					count: item.count,
					posts: [post],
				};
			})));
		} catch (err) {
			pino.error('Unable to retrieve trending hashtags');
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Retrieves content data for query optimization
	 *
	 * @param {string} content The post content
	 * @returns {object} The content data fields
	 */
	getContentData (content) {
		const references = _.filter(content.match(/https?:\/\/[^(\s)]+/g), url => (/(\/?)posts(\/?)(\S+)?/gm).test(url));
		return {
			mentions: content.match(/@[^(\s|.|,)]+/g) || undefined,
			references: !_.isEmpty(references) ? references : undefined,
			hashtags: content.match(/#[^(\s)]+/g) || undefined,
		};
	}
}

module.exports = new PostData();
