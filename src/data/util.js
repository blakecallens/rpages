const _ = require('lodash');
const { DateTime } = require('luxon');

class DataUtil {
	/**
	 * Returns the language of the client
	 *
	 * @param {object} req The express request object
	 * @returns {string} The client's language
	 */
	getLanguage (req) {
		return _.head(req.acceptsLanguages());
	}

	/**
	 * Formats a date to a readable datetime string
	 *
	 * @param {Date} date The date to format
	 * @returns {string} The formatted date
	 */
	getReadableDate (date) {
		return DateTime.fromJSDate(date).toLocaleString(DateTime.DATETIME_SHORT);
	}

	/**
	 * Formats a date to a readable date string
	 *
	 * @param {Date} date The date to format
	 * @returns {string} The formatted date
	 */
	getShortDate (date) {
		return DateTime.fromJSDate(date).toLocaleString(DateTime.DATE_SHORT);
	}

	/**
	 * Builds pagination data for a mongoose-pagination result
	 *
	 * @param {object} result The pagination result
	 * @param {number} page The page of results
	 * @returns {object} The pagination data
	 */
	getPagination (result, page) {
		return {
			previousPage: page > 0 ? (page - 1).toString() : undefined,
			nextPage: result.total > result.offset + result.limit ? (page + 1).toString() : undefined,
		};
	}
}

module.exports = new DataUtil();
