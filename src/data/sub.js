const _ = require('lodash');
const { DateTime } = require('luxon');
const mongoose = require('mongoose');
const pino = require('pino')({
	name: 'SubData',
	prettyPrint: true,
});
const dataUtil = require('./util');
const subSchema = require('../schemas/sub');
const userSchema = require('../schemas/user');
const consts = require('../consts');

class SubData {
	constructor () {
		this.Sub = mongoose.model('Sub', subSchema);
		this.User = mongoose.model('User', userSchema);
	}

	/**
	 * Retrieves the subscriber count of the user
	 *
	 * @param {object|string} _id The _id of the user
	 * @param {boolean} getNew Whether to get the new sub count
	 * @returns {number} The user's subscriber count
	 */
	async getUserSubCount (_id, getNew = false) {
		try {
			const query = { subscribed: _id };
			if (getNew) {
				_.set(query, 'created', { $gt: DateTime.local().minus({ days: 1 }) });
			}
			return await this.Sub.countDocuments(query);
		} catch (err) {
			pino.error(`Unable to retrieve sub count for user ${_id}`);
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Retrieves whether a user is subscribed to another user
	 *
	 * @param {object|string} subscriber The _id of the subscriber
	 * @param {object|string} subscribed The _id of the subscribed
	 * @returns {boolean} Whether the user is subscribed
	 */
	async userIsSubscribed (subscriber, subscribed) {
		try {
			return (await this.Sub.countDocuments({ subscriber, subscribed })) > 0;
		} catch (err) {
			pino.error(`Unable to retrieve sub status for ${subscriber} -> ${subscribed}`);
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Retrieves a subscription by users
	 *
	 * @param {object|string} subscriber The _id of the subscriber
	 * @param {object|string} subscribed The _id of the subscribed
	 * @returns {boolean} Whether the user is subscribed
	 */
	async userShowingEmail (subscriber, subscribed) {
		try {
			const subscription = await this.Sub.findOne({ subscriber, subscribed }, { showEmail: 1 });
			return _.get(subscription, 'showEmail');
		} catch (err) {
			pino.error(`Unable to retrieve sub for ${subscriber} -> ${subscribed}`);
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Returns all the subscriptions for a user
	 *
	 * @param {object|string} _id The _id of the user
	 * @returns {object[]} The user's subscriptions
	 */
	async getAllUserSubscriptions (_id) {
		try {
			return await this.Sub.find({ subscriber: _id });
		} catch (err) {
			pino.error(`Unable to retrieve subscriptions for user ${_id}`);
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Returns all the subscribers for a user
	 *
	 * @param {object|string} _id The _id of the user
	 * @returns {object[]} The user's subscribers
	 */
	async getAllUserSubscribers (_id) {
		try {
			return await this.Sub.find({ subscribed: _id });
		} catch (err) {
			pino.error(`Unable to retrieve subscribers for user ${_id}`);
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Retrieves a page of user subscriptions from the DB
	 *
	 * @param {object} _id The _id of user
	 * @param {number} page The page number
	 * @param {object} options Retrieval options
	 * @returns {object[]} The users
	 */
	async getUserSubscriptions (_id, page, options = {}) {
		try {
			const opts = _.assignIn({
				sort: { created: -1 },
			}, options);

			let query = { subscriber: _id };
			if (!_.isEmpty(opts.search)) {
				const allSubs = await this.getAllUserSubscriptions(_id);
				const userQuery = { _id: { $in: _.map(allSubs, 'subscribed') } };

				if (opts.search.indexOf(' ') > -1) {
					const words = opts.search.split(' ');
					_.assignIn(userQuery, {
						firstName: { $regex: new RegExp(`.*${_.head(words)}.*`, 'i') },
						lastName: { $regex: new RegExp(`.*${_.tail(words).join(' ')}.*`, 'i') },
					});
				} else {
					_.assignIn(userQuery, {
						$or: [
							{ handle: { $regex: new RegExp(`.*${opts.search}.*`, 'i') } },
							{ firstName: { $regex: new RegExp(`.*${opts.search}.*`, 'i') } },
							{ lastName: { $regex: new RegExp(`.*${opts.search}.*`, 'i') } },
						],
					});
				}

				const allUsers = await this.User.find(userQuery, { _id: 1 });
				query = { subscriber: _id, subscribed: { $in: _.map(allUsers, '_id') } };
			}

			const result = await this.Sub.paginate(query, {
				offset: page * consts.PAGE_LIMIT,
				limit: consts.PAGE_LIMIT,
				sort: opts.sort,
				populate: 'subscribed',
				lean: true,
			});
			result.docs = _.map(result.docs, doc => {
				_.set(doc, 'subscribed.readableDate', dataUtil.getShortDate(doc.created));
				return _.omit(doc, 'subscribed.password');
			});
			return _.assignIn(result, dataUtil.getPagination(result, page));
		} catch (err) {
			pino.error('Unable to retrieve posts');
			pino.error(err);
			throw err;
		}
	}

	/**
	 * Retrieves a page of user subscribers from the DB
	 *
	 * @param {object} req The express request object
	 * @param {object} _id The _id of user
	 * @param {number} page The page number
	 * @param {object} options Retrieval options
	 * @returns {object[]} The users
	 */
	async getUserSubscribers (req, _id, page, options = {}) {
		try {
			const opts = _.assignIn({
				sort: { created: -1 },
			}, options);

			let query = { subscribed: _id };
			if (!_.isEmpty(opts.search)) {
				const allSubs = await this.getAllUserSubscribers(_id);
				const userQuery = { _id: { $in: _.map(allSubs, 'subscriber') } };

				if (opts.search.indexOf(' ') > -1) {
					const words = opts.search.split(' ');
					_.assignIn(userQuery, {
						firstName: { $regex: new RegExp(`.*${_.head(words)}.*`, 'i') },
						lastName: { $regex: new RegExp(`.*${_.tail(words).join(' ')}.*`, 'i') },
					});
				} else {
					_.assignIn(userQuery, {
						$or: [
							{ handle: { $regex: new RegExp(`.*${opts.search}.*`, 'i') } },
							{ firstName: { $regex: new RegExp(`.*${opts.search}.*`, 'i') } },
							{ lastName: { $regex: new RegExp(`.*${opts.search}.*`, 'i') } },
						],
					});
				}

				const allUsers = await this.User.find(userQuery, { _id: 1 });
				query = { subscribed: _id, subscriber: { $in: _.map(allUsers, '_id') } };
			}

			const result = await this.Sub.paginate(query, {
				offset: page * consts.PAGE_LIMIT,
				limit: consts.PAGE_LIMIT,
				sort: opts.sort,
				populate: 'subscriber',
				lean: true,
			});
			result.docs = _.map(result.docs, doc => _.omit(doc, 'subscriber.password'));
			await Promise.all(_.map(result.docs, async doc => _.assignIn(doc.subscriber, {
				notLoggedInUser: !doc.subscriber._id.equals(_.get(req, 'user._id')),
				subscribed: _.get(req, 'user') ? await this.userIsSubscribed(req.user._id, doc.subscriber._id) : undefined,
				readableDate: dataUtil.getShortDate(doc.created),
			})));
			return _.assignIn(result, dataUtil.getPagination(result, page));
		} catch (err) {
			pino.error('Unable to retrieve posts');
			pino.error(err);
			throw err;
		}
	}
}

module.exports = new SubData();
