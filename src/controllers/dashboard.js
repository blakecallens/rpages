const _ = require('lodash');
const pino = require('pino')({
	name: 'DashboardController',
	prettyPrint: true,
});
const getUser = require('../middleware/get-user');
const templateService = require('../services/template');
const postData = require('../data/post');
const subData = require('../data/sub');

class DashboardController {
	/**
	 * Renders the dashboard page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderDashboard (req, res) {
		try {
			const subs = await subData.getAllUserSubscriptions(req.user._id);
			const query = { user: { $in: _.map(subs, 'subscribed').concat(req.user._id) } };
			const posts = !_.isEmpty(subs)
				? await postData.getPosts(req, query, _.parseInt(_.get(req, 'query.page', 0)), { getReferences: true })
				: undefined;

			res.type('html');
			res.send(templateService.buildTemplate(req, 'dashboard', {
				posts,
				dashboardPage: true,
				feedTab: true,
				mentions: await postData.getMentionCount(req.user.handle, { $ne: req.user._id }),
				returnTo: 'dashboard',
				subMentions: await postData.getMentionCount(req.user.handle, { $in: _.map(subs, 'subscribed') }),
				newSubscribers: subs.length ? await subData.getUserSubCount(req.user._id, true) : undefined,
			}));
		} catch (err) {
			pino.error(`Unable to render the dashboard for user ${_.get(req, 'user._id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Renders the sub mentions page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderSubMentions (req, res) {
		try {
			const subs = await subData.getAllUserSubscriptions(req.user._id);
			const posts = !_.isEmpty(subs) ? await postData.getPosts(
				req,
				{ user: { $in: _.map(subs, 'subscribed') }, mentions: `@${_.get(req, 'user.handle')}` },
				_.parseInt(_.get(req, 'query.page', 0)),
			) : undefined;

			res.type('html');
			res.send(templateService.buildTemplate(req, 'dashboard', {
				posts,
				dashboardPage: true,
				subMentionsTab: true,
				mentions: await postData.getMentionCount(req.user.handle, { $ne: req.user._id }),
				returnTo: 'mentions/sub',
				subMentions: await postData.getMentionCount(req.user.handle, { $in: _.map(subs, 'subscribed') }),
				newSubscribers: subs.length ? await subData.getUserSubCount(req.user._id, true) : undefined,
			}));
		} catch (err) {
			pino.error(`Unable to render sub mentions for user ${_.get(req, 'user._id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Renders the mentions page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderMentions (req, res) {
		try {
			const subs = await subData.getAllUserSubscriptions(req.user._id);
			const posts = await postData
				.getPosts(req, { mentions: `@${_.get(req, 'user.handle')}` }, _.parseInt(_.get(req, 'query.page', 0)));

			res.type('html');
			res.send(templateService.buildTemplate(req, 'dashboard', {
				posts,
				dashboardPage: true,
				mentionsTab: true,
				mentions: await postData.getMentionCount(req.user.handle, { $ne: req.user._id }),
				returnTo: 'mentions',
				subMentions: await postData.getMentionCount(req.user.handle, { $in: _.map(subs, 'subscribed') }),
				newSubscribers: subs.length ? await subData.getUserSubCount(req.user._id, true) : undefined,
			}));
		} catch (err) {
			pino.error(`Unable to render mentions for user ${_.get(req, 'user._id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Renders the dashboard page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderFirehose (req, res) {
		try {
			const subs = await subData.getAllUserSubscriptions(req.user._id);
			const posts = await postData.getPosts(req, {}, _.parseInt(_.get(req, 'query.page', 0)), { getReferences: true });

			res.type('html');
			res.send(templateService.buildTemplate(req, 'dashboard', {
				posts,
				dashboardPage: true,
				firehoseTab: true,
				mentions: await postData.getMentionCount(req.user.handle, { $ne: req.user._id }),
				returnTo: 'firehose',
				subMentions: await postData.getMentionCount(req.user.handle, { $in: _.map(subs, 'subscribed') }),
				newSubscribers: subs.length ? await subData.getUserSubCount(req.user._id, true) : undefined,
			}));
		} catch (err) {
			pino.error(`Unable to render the firehose for user ${_.get(req, 'user._id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/dashboard', getUser)
			.get('/dashboard', this.renderDashboard.bind(this));
		router.use('/dashboard/mentions', getUser)
			.get('/dashboard/mentions', this.renderMentions.bind(this));
		router.use('/dashboard/mentions/sub', getUser)
			.get('/dashboard/mentions/sub', this.renderSubMentions.bind(this));
		router.use('/firehose', getUser)
			.get('/dashboard/firehose', this.renderFirehose.bind(this));
	}
}

module.exports = new DashboardController();
