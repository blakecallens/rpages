const _ = require('lodash');
const uuid = require('uuid');
const { DateTime } = require('luxon');
const mongoose = require('mongoose');
const pino = require('pino')({
	name: 'SignupController',
	prettyPrint: true,
});
const captcha = require('trek-captcha');
const consts = require('../consts');
const sessionCheck = require('../middleware/session-check');
const emailService = require('../services/email');
const i18nService = require('../services/i18n');
const redisService = require('../services/redis');
const templateService = require('../services/template');
const subSchema = require('../schemas/sub');
const userSchema = require('../schemas/user');
const userData = require('../data/user');

class SignupController {
	constructor () {
		this.Sub = mongoose.model('Sub', subSchema);
		this.User = mongoose.model('User', userSchema);
	}

	/**
	 * Renders the signup page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderSignup (req, res) {
		try {
			const captchaId = uuid.v4();
			const { token, buffer } = await captcha();

			await redisService.set(`${consts.CAPTCHA_PREFIX}${captchaId}`, token);
			await redisService.expire(`${consts.CAPTCHA_PREFIX}${captchaId}`, 0.1);

			res.type('html');
			res.send(templateService.buildTemplate(req, 'signup', {
				captchaId,
				captchaImage: buffer.toString('base64'),
				dobMax: DateTime.local().minus({ years: consts.MIN_AGE }).toISODate(),
			}));
		} catch (err) {
			pino.error('Error rendering signup page');
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles signup attempts
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleSignup (req, res) {
		const language = _.head(req.acceptsLanguages());
		try {
			if (!_.get(req, 'body.dateOfBirth') || DateTime.fromISO(req.body.dateOfBirth) > DateTime.local().minus({ years: consts.MIN_AGE })) {
				this.handleInvalidSignup(req, res, 400, i18nService.getValue(language, 'INVALID_DATE_OF_BIRTH'));
				return;
			}

			const captchaToken = await redisService.get(`${consts.CAPTCHA_PREFIX}${_.get(req, 'query.captchaId')}`);
			await redisService.delete(`${consts.CAPTCHA_PREFIX}${_.get(req, 'query.captchaId')}`);
			if (_.isEmpty(captchaToken) || !_.get(req, 'body.captcha') || req.body.captcha !== captchaToken) {
				this.handleInvalidSignup(req, res, 400, i18nService.getValue(language, 'INVALID_CAPTCHA'));
				return;
			}

			if (!_.get(req, 'body.handle')) {
				const handle = (`${_.get(req, 'body.firstName')}${_.get(req, 'body.lastName', '')}`).toLowerCase().replace(/\s/g, '');
				if (await this.User.countDocuments({ handle }) === 0) {
					_.set(req, 'body.handle', handle);
				} else {
					_.set(req, 'body.handle', `handle${_.parseInt(Math.random() * 99999999)}`);
				}
			}

			const newUser = await this.User.create(req.body);

			const token = uuid.v4();
			await redisService.set(`${consts.EMAIL_VERIFICATION_PREFIX}${token}`, newUser._id.toString());
			await redisService.expire(`${consts.EMAIL_VERIFICATION_PREFIX}${token}`, consts.SESSION_EXPIRY_HOURS);

			// Skip email validation on localhost
			if (consts.HOST.indexOf('localhost') === -1) {
				await emailService.sendEmailVerification(req, newUser.email, token);
			} else {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'verification-success'));
				return;
			}

			res.type('html');
			res.send(templateService.buildTemplate(req, 'verification-email-sent'));
		} catch (err) {
			if (_.get(err, 'name') === 'ValidationError') {
				this.handleInvalidSignup(req, res, 400, i18nService.getValue(language, 'INVALID_FIELDS'));
			} else if (_.get(err, 'code') === 11000) {
				this.handleInvalidSignup(req, res, 409, i18nService.getValue(language, 'DUPLICATE_USER'));
			} else {
				pino.error(err);
				this.handleInvalidSignup(req, res, 500, i18nService.getValue(language, 'INTERNAL_ERROR'));
			}
		}
	}

	/**
	 * Renders the resend verification email page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	renderResendVerificationEmail (req, res) {
		res.type('html');
		res.send(templateService.buildTemplate(req, 'resend-email-verification'));
	}

	/**
	 * Handles resending the verification email
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleResendVerificationEmail (req, res) {
		try {
			const user = await userData.getUserByEmail(_.get(req, 'body.email'));
			if (!_.isEmpty(user) && !user.verified) {
				const token = uuid.v4();
				await redisService.set(`${consts.EMAIL_VERIFICATION_PREFIX}${token}`, user._id.toString());
				await redisService.expire(`${consts.EMAIL_VERIFICATION_PREFIX}${token}`, consts.SESSION_EXPIRY_HOURS);

				await emailService.sendEmailVerification(req, user.email, token);
			}

			res.type('html');
			res.send(templateService.buildTemplate(req, 'verification-email-sent'));
		} catch (err) {
			pino.error('Error handling forgot password start');
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles verifying an email verification token
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleEmailVerification (req, res) {
		try {
			const _id = await redisService.get(`${consts.EMAIL_VERIFICATION_PREFIX}${_.get(req, 'query.token')}`);
			if (_.isEmpty(_id)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'resend-email-verification', { invalidToken: true }));
				return;
			}
			const user = await userData.getUser(_id);
			if (_.isEmpty(user)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'resend-email-verification', { invalidToken: true }));
				return;
			}

			await this.User.updateOne({ _id: user._id }, { $set: { verified: true } });
			await redisService.delete(`${consts.EMAIL_VERIFICATION_PREFIX}${req.query.token}`);

			const blake = await this.User.findOne({ handle: 'blake' });
			if (!_.isEmpty(blake)) {
				await this.Sub.create({ subscriber: user._id, subscribed: blake._id });
			}

			res.type('html');
			res.send(templateService.buildTemplate(req, 'verification-success'));
		} catch (err) {
			pino.error(`Error handling email verification for token ${_.get(req, 'query.token')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles a bad signup request
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 * @param {number} code The HTTP response code
	 * @param {string} invalidSignupReason i18n key of the reson for the error
	 */
	async handleInvalidSignup (req, res, code, invalidSignupReason) {
		try {
			const captchaId = uuid.v4();
			const { token, buffer } = await captcha();

			await redisService.set(`${consts.CAPTCHA_PREFIX}${captchaId}`, token);
			await redisService.expire(`${consts.CAPTCHA_PREFIX}${captchaId}`, 0.25);

			res.type('html');
			res.status(code);
			res.send(templateService.buildTemplate(req, 'signup', _.assignIn(
				_.pick(req.body, ['email', 'firstName', 'lastName', 'dateOfBirth', 'invalidSignup', 'invalidSignupReason']),
				{
					captchaId,
					captchaImage: buffer.toString('base64'),
					invalidSignupReason,
					invalidSignup: true,
					dobMax: DateTime.local().minus({ years: consts.MIN_AGE }).toISODate(),
				},
			)));
		} catch (err) {
			pino.error('Error rendering invalid signup page');
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Builds the routes for the controller
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/signup', sessionCheck)
			.get('/signup', this.renderSignup.bind(this))
			.post('/signup', this.handleSignup.bind(this));
		router.get('/signup/verify-email', this.handleEmailVerification.bind(this));
		router.use('/signup/resend-verification', sessionCheck)
			.get('/signup/resend-verification', this.renderResendVerificationEmail.bind(this))
			.post('/signup/resend-verification', this.handleResendVerificationEmail.bind(this));
		router.get('/signup/verification', (req, res) => {
			res.type('html');
			res.send(templateService.buildTemplate(req, 'verification-email-sent'));
		});
	}
}

module.exports = new SignupController();
