const _ = require('lodash');
const uuid = require('uuid');
const mongoose = require('mongoose');
const pino = require('pino')({
	name: 'PasswordController',
	prettyPrint: true,
});
const consts = require('../consts');
const emailService = require('../services/email');
const redisService = require('../services/redis');
const templateService = require('../services/template');
const userSchema = require('../schemas/user');
const userData = require('../data/user');

class PasswordController {
	constructor () {
		this.User = mongoose.model('User', userSchema);
	}

	/**
	 * Renders the forgot password page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	renderForgotPasswordStart (req, res) {
		res.type('html');
		res.send(templateService.buildTemplate(req, 'forgot-password'));
	}

	/**
	 * Handles sending the forgotten password email
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleForgotPasswordStart (req, res) {
		try {
			const user = await userData.getUserByEmail(_.get(req, 'body.email'));
			if (!_.isEmpty(user)) {
				const token = uuid.v4();
				await redisService.set(`${consts.PASSWORD_RESET_PREFIX}${token}`, user._id.toString());
				await redisService.expire(`${consts.PASSWORD_RESET_PREFIX}${token}`, consts.SESSION_EXPIRY_HOURS);

				await emailService.sendPasswordReset(req, user.email, token);
			}

			res.type('html');
			res.send(templateService.buildTemplate(req, 'forgot-password-started'));
		} catch (err) {
			pino.error('Error handling forgot password start');
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles rendering the reset password page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderForgotPasswordFinish (req, res) {
		try {
			const _id = await redisService.get(`${consts.PASSWORD_RESET_PREFIX}${_.get(req, 'query.token')}`);
			if (_.isEmpty(_id)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'forgot-password', { invalidToken: true }));
				return;
			}
			const user = await userData.getUser(_id);
			if (_.isEmpty(user)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'forgot-password', { invalidToken: true }));
				return;
			}

			res.type('html');
			res.send(templateService.buildTemplate(req, 'forgot-password-finish', { token: req.query.token }));
		} catch (err) {
			pino.error('Error rendering forgot password finish');
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles finishing forgotten password flow
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleForgotPasswordFinish (req, res) {
		try {
			const _id = await redisService.get(`${consts.PASSWORD_RESET_PREFIX}${_.get(req, 'query.token')}`);
			if (_.isEmpty(_id)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'forgot-password', { invalidToken: true }));
			}
			const user = await userData.getUser(_id);
			if (_.isEmpty(user)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'forgot-password', { invalidToken: true }));
			}

			if (!_.get(req, 'body.newPassword')) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'forgot-password-finish', { token: req.query.token, invalidPassword: true }));
			}

			user.password = req.body.newPassword;
			user.verified = true;
			await user.save();
			await redisService.delete(`${consts.PASSWORD_RESET_PREFIX}${req.query.token}`);

			res.type('html');
			res.send(templateService.buildTemplate(req, 'forgot-password-success'));
		} catch (err) {
			pino.error('Error handling forgot password finish');
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.get('/forgot-password', this.renderForgotPasswordStart.bind(this))
			.post('/forgot-password', this.handleForgotPasswordStart.bind(this));
		router.get('/forgot-password/finish', this.renderForgotPasswordFinish.bind(this))
			.post('/forgot-password/finish', this.handleForgotPasswordFinish.bind(this));
	}
}

module.exports = new PasswordController();
