const _ = require('lodash');
const mongoose = require('mongoose');
const pino = require('pino')({
	name: 'PostsController',
	prettyPrint: true,
});
const consts = require('../consts');
const getUser = require('../middleware/get-user');
const getUserPassive = require('../middleware/get-user-passive');
const postService = require('../services/post');
const redisService = require('../services/redis');
const templateService = require('../services/template');
const dataUtil = require('../data/util');
const postSchema = require('../schemas/post');
const postData = require('../data/post');
const subData = require('../data/sub');

class PostsController {
	constructor () {
		this.Post = mongoose.model('Post', postSchema);
	}

	/**
	 * Renders the posts page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderPosts (req, res) {
		try {
			const posts = await postData.getPosts(req, { user: _.get(req, 'user._id') }, _.parseInt(_.get(req, 'query.page', 0)), {
				getReferences: true,
				sort: { pinned: -1, created: -1 },
			});

			res.type('html');
			res.send(templateService.buildTemplate(req, 'posts', {
				posts,
				user: _.get(req, 'user'),
				loggedIn: true,
				postsPage: true,
				maxChars: consts.MAX_POST_LENGTH,
			}));
		} catch (err) {
			pino.error(`Unable to render the posts page for user ${_.get(req, 'user._id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Renders a single post
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderPostDetail (req, res) {
		try {
			let post;
			try {
				post = await postData.getPost(req, _.get(req, 'params.id'), { getReferences: true });
			} catch (err) {
				pino.warn(`Unable to render post detail for _id ${_.get(req, 'params.id')}`);
			}
			if (_.isEmpty(post)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'not-found'));
				return;
			}

			post.user.memberSince = dataUtil.getShortDate(post.user.created);
			if (_.get(req, 'user')) {
				post.user.subscribers = await subData.getUserSubCount(post.user._id);
			}
			let posts = [post];

			if (_.get(req, 'user')) {
				const yourReferences = await this.Post.find({ user: req.user._id, references: { $regex: `.*${post._id}.*` } }).populate('user');
				if (!_.isEmpty(yourReferences)) {
					posts = posts.concat(await Promise.all(_.map(yourReferences, async reference => {
						_.set(reference, 'user', reference.user.clean());
						const objectRef = reference.toObject();

						return _.assignIn(objectRef, {
							createdISO: objectRef.created.toISOString(),
							content: await postService.replaceUrls(req, objectRef.content, true),
							canDelete: true,
						});
					})));
				}
			}

			const notLoggedInUser = _.get(req, 'user') && !post.canDelete;
			const templateValues = {
				posts,
				notLoggedInUser,
				subscribed: notLoggedInUser
					? await subData.userIsSubscribed(req.user._id, post.user._id) : undefined,
				user: post.user,
				isBlake: post.user.handle === 'blake',
				postsPage: true,
				returnTo: `posts/${post._id}`,
			};

			res.type('html');
			res.send(templateService.buildTemplate(req, 'post-detail', templateValues));
		} catch (err) {
			pino.error(`Unable to render the post details page for user ${_.get(req, 'params.id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Renders a single post as embeddabe
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderPostEmbed (req, res) {
		try {
			const post = await postData.getPost(req, _.get(req, 'params.id'), { getReferences: true });
			if (_.isEmpty(post)) {
				res.type('html');
				res.type('html').status(404).send(templateService.buildTemplate(req, 'post-embed'));
				return;
			}

			res.type('html');
			res.send(templateService.buildTemplate(req, 'post-embed', { post }));
		} catch (err) {
			pino.error(`Unable to render the post embed page for post ${_.get(req, 'params.id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Renders a post's references
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderPostReferences (req, res) {
		try {
			const post = await postData.getPost(req, _.get(req, 'params.id'));
			if (_.isEmpty(post)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'not-found', { loggedIn: !_.isEmpty(req.user) }));
				return;
			}

			const references = await postData.getReferences(req, post._id, _.parseInt(_.get(req, 'query.page', 0)));

			res.type('html');
			res.send(templateService.buildTemplate(req, 'post-detail', {
				references,
				posts: [post],
				postsPage: true,
				referencesPage: true,
				returnTo: `posts/${post._id}`,
			}));
		} catch (err) {
			pino.error(`Unable to render the post references page for post ${_.get(req, 'params.id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Renders the post preview page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderPreviewPost (req, res) {
		try {
			const post = {
				user: _.get(req, 'user'),
				created: new Date(),
				source: _.get(req, 'body.content'),
				content: await postService.replaceUrls(req, _.get(req, 'body.content').replace(/<[^>]*>/g, '')),
				createdISO: new Date().toISOString(),
				readableDate: dataUtil.getReadableDate(new Date()),
			};

			res.type('html');
			res.send(templateService.buildTemplate(req, 'post-detail', {
				preview: true,
				notLoggedInUser: false,
				content: _.get(req, 'body.content'),
				posts: [post],
				postsPage: true,
				returnTo: '/posts',
			}));
		} catch (err) {
			pino.error(`Unable to preview a post for user ${_.get(req, 'user._id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles post creation
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleNewPost (req, res) {
		try {
			const redirectUrl = _.head(_.get(req, 'query.returnTo', '/posts').split('?'));
			const postLimitKey = `${consts.POST_LIMIT_PREFIX}${req.user._id}`;
			if (await redisService.get(postLimitKey)) {
				res.redirect(`${redirectUrl}?postLimit=true`);
				return;
			}

			const post = _.assignIn(_.clone(req.body), { user: _.get(req, 'user._id'), created: new Date() });
			await this.Post.create(_.assignIn(post, postData.getContentData(post.content)));

			await redisService.set(postLimitKey, 'true');
			await redisService.expire(postLimitKey, 0.05);

			res.redirect(`${redirectUrl}?postSuccess=true`);
		} catch (err) {
			pino.error(`Unable to create a new post for user ${_.get(req, 'user._id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles pinning a post.
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handlePinPost (req, res) {
		try {
			await this.Post.updateMany({ user: _.get(req, 'user._id'), pinned: true }, { $set: { pinned: false } });
			await this.Post.updateOne({ _id: _.get(req, 'params.id'), user: _.get(req, 'user._id') }, { $set: { pinned: true } });
			res.redirect(_.head(_.get(req, 'query.returnTo', '/posts').split('?')));
		} catch (err) {
			pino.error(`Unable to pin post ${_.get(req, 'params.id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles deleting a post
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleDeletePost (req, res) {
		try {
			const post = await this.Post.findOne({ _id: _.get(req, 'params.id'), user: _.get(req, 'user._id') });
			if (!_.isEmpty(post)) {
				await this.Post.deleteOne(post);
			}
			res.redirect(_.head(_.get(req, 'query.returnTo', '/posts').split('?')));
		} catch (err) {
			pino.error(`Unable to delete post ${_.get(req, 'params.id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Renders a single post
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderReferencePost (req, res) {
		try {
			const post = await postData.getPost(req, _.get(req, 'params.id'), {});
			if (_.isEmpty(post)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'not-found'));
				return;
			}

			res.type('html');
			res.send(templateService.buildTemplate(req, 'posts', {
				posts: { docs: [post] },
				user: _.get(req, 'user'),
				loggedIn: true,
				postsPage: true,
				maxChars: consts.MAX_POST_LENGTH,
				content: `@${post.user.handle}, re: https://${consts.HOST}/posts/${post._id} `,
				returnTo: _.get(req, 'query.returnTo', 'posts'),
			}));
		} catch (err) {
			pino.error(`Unable to render the post details page for user ${_.get(req, 'params.id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/posts/preview', getUser)
			.post('/posts/preview', this.renderPreviewPost.bind(this));
		router.use('/posts/:id', getUserPassive)
			.get('/posts/:id', this.renderPostDetail.bind(this));
		router.get('/posts/:id/embed', this.renderPostEmbed.bind(this));
		router.use('/posts/:id/references', getUser)
			.get('/posts/:id/references', this.renderPostReferences.bind(this));
		router.use('/posts/:id/pin', getUser)
			.post('/posts/:id/pin', this.handlePinPost.bind(this));
		router.use('/posts/:id/delete', getUser)
			.get('/posts/:id/delete', this.handleDeletePost.bind(this));
		router.use('/posts/:id/reference', getUser)
			.get('/posts/:id/reference', this.renderReferencePost.bind(this));
		router.use('/posts', getUser)
			.get('/posts', this.renderPosts.bind(this))
			.post('/posts', this.handleNewPost.bind(this));
	}
}

module.exports = new PostsController();
