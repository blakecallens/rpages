const _ = require('lodash');
const getUserPassive = require('../middleware/get-user-passive');
const sessionCheck = require('../middleware/session-check');
const templateService = require('../services/template');

class SignupController {
	/**
	 * Renders the landing page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	renderLanding (req, res) {
		res.type('html');
		res.send(templateService.buildTemplate(req, 'landing'));
	}

	/**
	 * Renders the help page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	renderHelp (req, res) {
		const templateValues = { helpPage: true };
		_.set(templateValues, _.get(req, 'tab', 'aboutTab'), true);
		res.type('html');
		res.send(templateService.buildTemplate(req, 'help', templateValues));
	}

	/**
	 * Generates a middleware function to set the tab
	 *
	 * @param {object} req The Express request object
	 * @param {object} res The Express response object
	 * @param {Function} next The Express next function
	 */
	setTab (req, res, next) {
		req.tab = `${_.get(req, 'params.tab', 'about')}Tab`;
		next();
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.get('/offline', (req, res) => {
			res.type('html');
			res.send(templateService.buildTemplate(req, 'offline'));
		});
		router.use('/privacy-policy', getUserPassive)
			.get('/privacy-policy', (req, res) => {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'privacy'));
			});
		router.use('/terms-and-conditions', getUserPassive)
			.get('/terms-and-conditions', (req, res) => {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'terms'));
			});
		router.get('/help', (req, res) => res.redirect('/help/about'));
		router.use('/help/:tab', getUserPassive).use('/help/:tab', this.setTab)
			.get('/help/:tab', this.renderHelp.bind(this));
		router.get('/sitemap.xml', (req, res) => {
			res.type('xml');
			res.send(templateService.buildTemplate(req, 'sitemap'));
		});
		router.use('/', sessionCheck)
			.get('/', this.renderLanding.bind(this));
		router.get('*', (req, res) => {
			res.type('html');
			res.send(templateService.buildTemplate(req, 'not-found'));
		});
	}
}

module.exports = new SignupController();
