const _ = require('lodash');
const pino = require('pino')({
	name: 'AccountsController',
	prettyPrint: true,
});
const mongoose = require('mongoose');
const consts = require('../consts');
const fileUpload = require('../middleware/file-upload');
const getUser = require('../middleware/get-user');
const i18nService = require('../services/i18n');
const imageService = require('../services/image');
const cookieService = require('../services/cookie');
const emailService = require('../services/email');
const redisService = require('../services/redis');
const templateService = require('../services/template');
const subSchema = require('../schemas/sub');
const postSchema = require('../schemas/post');
const userSchema = require('../schemas/user');
const dataUtil = require('../data/util');
const userData = require('../data/user');

class AccountController {
	constructor () {
		this.User = mongoose.model('User', userSchema);
		this.Sub = mongoose.model('Sub', subSchema);
		this.Post = mongoose.model('Post', postSchema);
	}

	/**
	 * Renders the account page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderAccountPage (req, res) {
		try {
			const user = await userData.getUser(req.user._id);
			const templateValues = {
				user: user.clean(['customCss', 'spotifyUrl', 'imgur']),
				accountPage: true,
				maxChars: _.get(req, 'tab') === 'profileTab' ? consts.MAX_ABOUT_LENGTH : consts.MAX_CSS_LENGTH,
			};
			_.set(templateValues, _.get(req, 'tab', 'profileTab'), true);

			res.type('html');
			res.send(templateService.buildTemplate(req, 'account', templateValues));
		} catch (err) {
			pino.error(`Unable to render the account page for user ${_.get(req, 'user._id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles updating account info
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleUpdateAccount (req, res) {
		const body = _.pick(_.get(req, 'body'), ['handle', 'firstName', 'lastName', 'website', 'about']);
		try {
			body.handle = body.handle.replace(/(@|\/)/g, '');
			await this.User.updateOne({ _id: req.user._id }, { $set: body });

			res.type('html');
			res.send(templateService.buildTemplate(req, 'account', {
				successfulUpdate: true,
				user: _.assignIn(req.user, body),
				accountPage: true,
				profileTab: true,
				maxChars: consts.MAX_ABOUT_LENGTH,
			}));
		} catch (err) {
			pino.error(`Unable to update account ${_.get(req, 'user._id')}`);
			pino.error(err);
			const language = dataUtil.getLanguage(req);
			if (err.code === 11000) {
				this.handleInvalidUpdateAccount(
					req, res, 'profileTab', 409, i18nService.getValue(language, 'DUPLICATE_HANDLE', _.pick(body, 'handle')),
				);
			} else {
				this.handleInvalidUpdateAccount(req, res, 'profileTab', 500, i18nService.getValue(language, 'INTERNAL_ERROR'));
			}
		}
	}

	/**
	 * Handles updating user image
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleUpdateImage (req, res) {
		try {
			if (req.fileError) {
				switch (req.fileError.code) {
					case 'LIMIT_FILE_SIZE':
						this.handleInvalidUpdateAccount(req, res, 'imageTab', 400, i18nService.getValue(dataUtil.getLanguage(req), 'FILE_TOO_LARGE'));
						return;
					default:
						throw new Error();
				}
			}

			const image = await imageService.resizeAndSaveUserImage(req.file.buffer);
			await this.User.updateOne({ _id: req.user._id }, { $set: { image } });

			if (_.get(req, 'user.image')) {
				imageService.removeUserImage(req.user.image);
			}

			res.type('html');
			res.send(templateService.buildTemplate(req, 'account', {
				successfulUpdate: true,
				user: _.assignIn(req.user, { image }),
				accountPage: true,
				imageTab: true,
				maxChars: consts.MAX_ABOUT_LENGTH,
			}));
		} catch (err) {
			pino.error(`Unable to update account ${_.get(req, 'user._id')}`);
			pino.error(err);
			this.handleInvalidUpdateAccount(req, res, 'imageTab', 500, i18nService.getValue(dataUtil.getLanguage(req), 'INTERNAL_ERROR'));
		}
	}

	/**
	 * Handles rotating user image
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleRotateImage (req, res) {
		try {
			const user = await userData.getUser(req.user._id);
			let image = _.get(user, 'image');
			if (!_.isEmpty(image)) {
				image = imageService.rotateImage(image);
				await this.User.updateOne({ _id: user._id }, { $set: { image } });
			}

			res.type('html');
			res.send(templateService.buildTemplate(req, 'account', {
				user: _.assignIn(req.user, { image }),
				accountPage: true,
				imageTab: true,
				maxChars: consts.MAX_ABOUT_LENGTH,
			}));
		} catch (err) {
			pino.error(`Unable to update account ${_.get(req, 'user._id')}`);
			pino.error(err);
			this.handleInvalidUpdateAccount(req, res, 'imageTab', 500, i18nService.getValue(dataUtil.getLanguage(req), 'INTERNAL_ERROR'));
		}
	}

	/**
	 * Handles updating user custom CSS
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleUpdateCustomization (req, res) {
		try {
			let customCss = _.get(req, 'body.customCss');
			if (!_.isEmpty(customCss)) {
				customCss = customCss.replace(/<[^>]*>/g, '');
			}
			const spotifyUrl = _.get(req, 'body.spotifyUrl');
			const darkMode = _.get(req, 'body.darkMode') === 'on';
			const hideNonSubMentions = _.get(req, 'body.hideNonSubMentions') === 'on';
			const showFirehose = _.get(req, 'body.showFirehose') === 'on';

			if (!_.isEmpty(spotifyUrl) && !spotifyUrl.match(/https:\/\/open\.spotify\.com\//) && !spotifyUrl.match(/soundcloud\.com/)) {
				this.handleInvalidUpdateAccount(
					req, res, 'customizeTab', 400, i18nService.getValue(dataUtil.getLanguage(req), 'INVALID_SPOTIFY_URL'),
				);
				return;
			}

			await this.User.updateOne({ _id: _.get(req, 'user._id') }, {
				$set: {
					customCss,
					spotifyUrl,
					darkMode,
					hideNonSubMentions,
					showFirehose,
				},
			});

			res.type('html');
			res.send(templateService.buildTemplate(req, 'account', {
				successfulUpdate: true,
				user: _.assignIn(req.user, {
					customCss,
					spotifyUrl,
					darkMode,
					hideNonSubMentions,
					showFirehose,
				}),
				accountPage: true,
				customizeTab: true,
				maxChars: consts.MAX_CSS_LENGTH,
			}));
		} catch (err) {
			pino.error(`Unable to update account customization ${_.get(req, 'user._id')}`);
			const language = dataUtil.getLanguage(req);
			pino.error(err);
			this.handleInvalidUpdateAccount(req, res, 'passwordTab', 500, i18nService.getValue(language, 'INTERNAL_ERROR'));
		}
	}

	/**
	 * Handles updating user password
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleUpdatePassword (req, res) {
		try {
			const user = await userData.getUser(req.user._id);
			if (_.isEmpty(user) || !_.get(req, 'body.newPassword') || !await user.comparePassword(req.body.password)) {
				this.handleInvalidUpdateAccount(req, res, 'passwordTab', 400, i18nService.getValue(dataUtil.getLanguage(req), 'INVALID_PASSWORD'));
				return;
			}

			user.password = req.body.newPassword;
			await user.save();

			res.type('html');
			res.send(templateService.buildTemplate(req, 'account', {
				successfulUpdate: true,
				accountPage: true,
				passwordTab: true,
				maxChars: consts.MAX_ABOUT_LENGTH,
			}));
		} catch (err) {
			pino.error(err);
			this.handleInvalidUpdateAccount(req, res, 'passwordTab', 500, i18nService.getValue(dataUtil.getLanguage(req), 'INTERNAL_ERROR'));
		}
	}

	/**
	 * Handles updating user email
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleUpdateEmail (req, res) {
		try {
			const user = await userData.getUser(req.user._id);
			if (_.isEmpty(user) || _.get(req, 'body.email').toLowerCase() !== user.email || !_.get(req, 'body.newEmail') || req.body.email === req.body.newEmail) {
				this.handleInvalidUpdateAccount(req, res, 'emailTab', 400, i18nService.getValue(dataUtil.getLanguage(req), 'INVALID_FIELDS'));
				return;
			}
			if (!_.isEmpty(await userData.getUserByEmail(req.body.newEmail))) {
				this.handleInvalidUpdateAccount(req, res, 'emailTab', 400, i18nService.getValue(dataUtil.getLanguage(req), 'DUPLICATE_USER'));
				return;
			}

			let token = '';
			_.times(4, () => {
				token += Math.floor(Math.random() * 10);
			});
			const data = {
				userId: user._id,
				newEmail: req.body.newEmail,
			};
			await redisService.set(`${consts.EMAIL_VERIFICATION_PREFIX}${token}`, data);
			await redisService.expire(`${consts.EMAIL_VERIFICATION_PREFIX}${token}`, 1);

			await emailService.sendEmailChange(req, req.body.newEmail, token);

			res.type('html');
			res.send(templateService.buildTemplate(req, 'account', {
				verification: true,
				accountPage: true,
				emailTab: true,
			}));
		} catch (err) {
			const language = dataUtil.getLanguage(req);
			pino.error(err);
			this.handleInvalidUpdateAccount(req, res, 'passwordTab', 500, i18nService.getValue(language, 'INTERNAL_ERROR'));
		}
	}

	/**
	 * Handles verifying user email
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleVerifyEmail (req, res) {
		try {
			const data = await redisService.get(`${consts.EMAIL_VERIFICATION_PREFIX}${_.get(req, 'body.token')}`);
			if (_.isEmpty(data)) {
				this.handleInvalidUpdateAccount(req, res, 'emailTab', 400, i18nService.getValue(dataUtil.getLanguage(req), 'INVALID_CODE'), { verification: true });
				return;
			}
			const user = await userData.getUser(data.userId);
			if (_.isEmpty(user)) {
				this.handleInvalidUpdateAccount(req, res, 'emailTab', 400, i18nService.getValue(dataUtil.getLanguage(req), 'INVALID_CODE'), { verification: true });
				return;
			}

			await this.User.updateOne({ _id: user._id }, { $set: { email: data.newEmail } });
			await redisService.delete(`${consts.EMAIL_VERIFICATION_PREFIX}${req.body.token}`);

			res.type('html');
			res.send(templateService.buildTemplate(req, 'account', {
				successfulUpdate: true,
				accountPage: true,
				emailTab: true,
			}));
		} catch (err) {
			const language = dataUtil.getLanguage(req);
			pino.error(err);
			this.handleInvalidUpdateAccount(req, res, 'passwordTab', 500, i18nService.getValue(language, 'INTERNAL_ERROR'));
		}
	}

	/**
	 * Handles downloading account data
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleAccountDownload (req, res) {
		try {
			let subscriptions = await this.Sub.find({ subscriber: req.user._id }).populate(['subscriber', 'subscribed']);
			subscriptions = _.map(subscriptions, sub => ({
				subscriber: sub.subscriber.handle,
				subscribed: sub.subscribed.handle,
				created: sub.created,
			}));
			let subscribers = await this.Sub.find({ subscribed: req.user._id }).populate(['subscriber', 'subscribed']);
			subscribers = _.map(subscribers, sub => ({
				subscriber: sub.subscriber.handle,
				subscribed: sub.subscribed.handle,
				created: sub.created,
			}));
			let posts = await this.Post.find({ user: req.user._id }).populate('user');
			posts = _.map(posts, post => _.omit(post.toObject(), ['user', '__v']));
			let user = await userData.getUser(req.user._id);
			user = user.clean(['customCss', 'spotifyUrl', 'lastLogin']);

			const jsonDump = {
				user,
				subscriptions,
				subscribers,
				posts,
			};

			res.set('Content-Disposition', 'attachment; filename="data.json"');
			res.send(Buffer.from(JSON.stringify(jsonDump)));
		} catch (err) {
			pino.error(`Unable to download data for ${_.get(req, 'user._id')}`);
			const language = dataUtil.getLanguage(req);
			pino.error(err);
			this.handleInvalidUpdateAccount(req, res, 'deleteTab', 500, i18nService.getValue(language, 'INTERNAL_ERROR'));
		}
	}

	/**
	 * Handles deleting a user account
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleDeleteAccount (req, res) {
		try {
			await this.Sub.deleteMany({
				$or: [
					{ subscriber: req.user._id },
					{ subscribed: req.user._id },
				],
			});
			await this.Post.deleteMany({ user: req.user._id });
			await this.User.deleteOne({ _id: req.user._id });

			if (_.get(req, 'user.image')) {
				imageService.removeUserImage(req.user.image);
			}

			cookieService.delete(res, consts.SESSION_COOKIE_NAME);
			await redisService.delete(`${consts.SESSION_PREFIX}${_.get(req, ['cookies', consts.SESSION_COOKIE_NAME])}`);
			res.redirect('/');
		} catch (err) {
			pino.error(`Unable to update account customization ${_.get(req, 'user._id')}`);
			const language = dataUtil.getLanguage(req);
			pino.error(err);
			this.handleInvalidUpdateAccount(req, res, 'deleteTab', 500, i18nService.getValue(language, 'INTERNAL_ERROR'));
		}
	}

	/**
	 * Handles a bad update request
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 * @param {string} tab The name of the tab to return to
	 * @param {number} code The HTTP response code
	 * @param {string} invalidUpdateAccountReason i18n key of the reson for the error
	 * @param {object} [fields] Optional template fields
	 */
	handleInvalidUpdateAccount (req, res, tab, code, invalidUpdateAccountReason, fields = {}) {
		_.set(fields, tab, true);
		res.type('html');
		res.status(code);
		res.send(templateService.buildTemplate(req, 'account', _.assignIn({
			invalidUpdateAccountReason,
			invalidUpdate: true,
			user: _.assignIn(_.get(req, 'user'), _.get(req, 'body')),
			accountPage: true,
			maxChars: consts.MAX_ABOUT_LENGTH,
		}, fields)));
	}

	/**
	 * Generates a middleware function to set the tab
	 *
	 * @param {string} tab The name of the tab
	 * @returns {Function} The middleware function
	 */
	setTab (tab) {
		return (req, res, next) => {
			req.tab = tab;
			next();
		};
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/account', getUser).use(this.setTab('profileTab'))
			.get('/account', this.renderAccountPage.bind(this))
			.post('/account', this.handleUpdateAccount.bind(this));
		router.use('/account/image', getUser).use(this.setTab('imageTab')).use(fileUpload)
			.get('/account/image', this.renderAccountPage.bind(this))
			.post('/account/image', this.handleUpdateImage.bind(this));
		router.use('/account/image/rotate', getUser).use(this.setTab('imageTab'))
			.post('/account/image/rotate', this.handleRotateImage.bind(this));
		router.use('/account/customize', getUser).use(this.setTab('customizeTab'))
			.get('/account/customize', this.renderAccountPage.bind(this))
			.post('/account/customize', this.handleUpdateCustomization.bind(this));
		router.use('/account/password', getUser).use(this.setTab('passwordTab'))
			.get('/account/password', this.renderAccountPage.bind(this))
			.post('/account/password', this.handleUpdatePassword.bind(this));
		router.use('/account/email', getUser).use(this.setTab('emailTab'))
			.get('/account/email', this.renderAccountPage.bind(this))
			.post('/account/email', this.handleUpdateEmail.bind(this));
		router.use('/account/email/verify', getUser).use(this.setTab('emailTab'))
			.post('/account/email/verify', this.handleVerifyEmail.bind(this));
		router.use('/account/download', getUser).use(this.setTab('downloadTab'))
			.get('/account/download', this.renderAccountPage.bind(this))
			.post('/account/download', this.handleAccountDownload.bind(this));
		router.use('/account/delete', getUser).use(this.setTab('deleteTab'))
			.get('/account/delete', this.renderAccountPage.bind(this))
			.post('/account/delete', this.handleDeleteAccount.bind(this));
	}
}

module.exports = new AccountController();
