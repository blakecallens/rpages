const _ = require('lodash');
const pino = require('pino')({
	name: 'GiphyController',
	prettyPrint: true,
});
const getUser = require('../middleware/get-user');
const giphyService = require('../services/giphy');
const templateService = require('../services/template');

class GiphyController {
	/**
	 * Handles signin attempts
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderGiphySearch (req, res) {
		try {
			const gifs = await giphyService.search(_.get(req, 'query.search'));

			res.type('html');
			res.send(templateService.buildTemplate(req, 'giphy-search', { gifs }));
		} catch (err) {
			pino.error('Unable to process giphy search');
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'giphy-search', { error: true }));
		}
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/giphy/search', getUser)
			.get('/giphy/search', this.renderGiphySearch.bind(this));
	}
}

module.exports = new GiphyController();
