const _ = require('lodash');
const uuid = require('uuid');
const mongoose = require('mongoose');
const pino = require('pino')({ prettyPrint: true });
const consts = require('../consts');
const sessionCheck = require('../middleware/session-check');
const userSchema = require('../schemas/user');
const cookieService = require('../services/cookie');
const redisService = require('../services/redis');
const templateService = require('../services/template');
const userData = require('../data/user');

class SigninController {
	constructor () {
		this.User = mongoose.model('User', userSchema);
	}

	/**
	 * Renders the signin page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	renderSignin (req, res) {
		const templateValues = {
			isSignin: true,
			returnTo: _.get(req, 'query.returnTo'),
		};
		res.type('html');

		if (_.get(req, 'body.invalidLogin') || _.get(req, 'body.unverified')) {
			_.assignIn(templateValues, _.pick(req.body, ['email', 'invalidLogin', 'unverified']));
			res.status(400);
		}

		res.send(templateService.buildTemplate(req, 'signin', templateValues));
	}

	/**
	 * Handles signin attempts
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleSignin (req, res) {
		try {
			const user = await userData.getUserByEmail(_.get(req, 'body.email'));
			if (_.isEmpty(user) || !await user.comparePassword(req.body.password)) {
				this.handleInvalidSignin(req, res);
				return;
			}
			if (!user.verified) {
				_.assignIn(req.body, { unverified: true });
				this.renderSignin(req, res);
				return;
			}

			const sessionId = uuid.v4();
			await redisService.set(`${consts.SESSION_PREFIX}${sessionId}`, user._id.toString());
			await redisService.expire(`${consts.SESSION_PREFIX}${sessionId}`, consts.SESSION_EXPIRY_HOURS);
			cookieService.set(
				res,
				consts.SESSION_COOKIE_NAME,
				sessionId,
				consts.SESSION_EXPIRY_HOURS * 60 * 60 * 1000,
			);

			await this.User.updateOne({ _id: user._id }, { $set: { lastLogin: new Date() } });
			res.redirect(_.get(req, 'query.returnTo', '/dashboard'));
		} catch (err) {
			pino.error('Unable to process signin');
			pino.error(err);
			this.handleInvalidSignin(req, res);
		}
	}

	/**
	 * Handles a bad signin attempt
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	handleInvalidSignin (req, res) {
		_.assignIn(req.body, { invalidLogin: true });
		this.renderSignin(req, res);
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/signin', sessionCheck)
			.get('/signin', this.renderSignin.bind(this))
			.post('/signin', this.handleSignin.bind(this));
	}
}

module.exports = new SigninController();
