const _ = require('lodash');
const pino = require('pino')({
	name: 'DashboardController',
	prettyPrint: true,
});
const getUser = require('../middleware/get-user');
const templateService = require('../services/template');
const postData = require('../data/post');

class DashboardController {
	/**
	 * Renders the trending page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderTrending (req, res) {
		try {
			const trending = await postData.getTrendingHashtags(req);

			res.type('html');
			res.send(templateService.buildTemplate(req, 'discover', {
				trending,
				discoverPage: true,
				trendingTab: true,
				returnTo: 'discover',
			}));
		} catch (err) {
			pino.error(`Unable to render trending for user ${_.get(req, 'user._id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Renders the trending users page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderTrendingUsers (req, res) {
		try {
			const trendingUsers = await postData.getTrendingUsers(req);

			res.type('html');
			res.send(templateService.buildTemplate(req, 'discover', {
				trendingUsers,
				discoverPage: true,
				trendingUsersTab: true,
				returnTo: 'discover',
			}));
		} catch (err) {
			pino.error(`Unable to render trending users for user ${_.get(req, 'user._id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Renders the active users page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderActiveUsers (req, res) {
		try {
			const activeUsers = await postData.getActiveUsers(req);

			res.type('html');
			res.send(templateService.buildTemplate(req, 'discover', {
				activeUsers,
				discoverPage: true,
				activeUsersTab: true,
				returnTo: 'discover',
			}));
		} catch (err) {
			pino.error(`Unable to render active users for user ${_.get(req, 'user._id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/discover', getUser)
			.get('/discover', this.renderTrending.bind(this));
		router.use('/discover/trending-users', getUser)
			.get('/discover/trending-users', this.renderTrendingUsers.bind(this));
		router.use('/discover/active-users', getUser)
			.get('/discover/active-users', this.renderActiveUsers.bind(this));
	}
}

module.exports = new DashboardController();
