const _ = require('lodash');
const pino = require('pino')({
	name: 'ImgurController',
	prettyPrint: true,
});
const mongoose = require('mongoose');
const request = require('request-promise-native');
const consts = require('../consts');
const fileUpload = require('../middleware/file-upload');
const getUser = require('../middleware/get-user');
const userSchema = require('../schemas/user');
const imageService = require('../services/image');
const templateService = require('../services/template');


class ImgurController {
	constructor () {
		this.User = mongoose.model('User', userSchema);
	}

	/**
	 * Renders the Imgur image list page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderImgurList (req, res) {
		try {
			if (!_.get(req, 'user.imgur')) {
				throw new Error('No Imgur auth for user');
			}
			if (new Date() > req.user.imgur.expires) {
				await this.refreshToken(req);
			}

			const page = _.parseInt(_.get(req, 'query.page', 0));
			const result = await request({
				method: 'GET',
				url: `https://api.imgur.com/3/account/${req.user.imgur.account_username}/images/${page}`,
				headers: { Authorization: `Bearer ${req.user.imgur.access_token}` },
				json: true,
			});

			res.type('html');
			res.send(templateService.buildTemplate(req, 'imgur-list', { images: result.data }));
		} catch (err) {
			pino.error('Unable to render Imgur list page');
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'imgur-list', { error: true }));
		}
	}

	/**
	 * Renders the Imgur upload page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	renderImgurUpload (req, res) {
		res.type('html');
		res.send(templateService.buildTemplate(req, 'imgur-upload'));
	}

	/**
	 * Handles Imgur upload
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleImgurUpload (req, res) {
		try {
			if (req.fileError) {
				switch (req.fileError.code) {
					case 'LIMIT_FILE_SIZE':
						res.type('html');
						res.send(templateService.buildTemplate(req, 'imgur-upload', { fileSizeError: true }));
						return;
					default:
						throw new Error();
				}
			}

			if (!_.get(req, 'user.imgur')) {
				throw new Error('No Imgur auth for user');
			}
			if (new Date() > req.user.imgur.expires) {
				await this.refreshToken(req);
			}

			let image = req.file.buffer;
			const dimensions = imageService.getImageBufferSize(image);
			if (dimensions.width > consts.MAX_IMAGE_WIDTH) {
				image = await imageService.resizeImage(req.file.buffer, consts.MAX_IMAGE_WIDTH);
			}
			await request({
				method: 'POST',
				url: 'https://api.imgur.com/3/upload',
				headers: { Authorization: `Bearer ${req.user.imgur.access_token}` },
				formData: { image },
			});

			res.redirect('/imgur/list');
		} catch (err) {
			pino.error('Unable to handle Imgur image upload');
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'imgur-upload', { error: true }));
		}
	}

	/**
	 * Refreshes a user's Imgur auth
	 *
	 * @param {object} req The express request object
	 */
	async refreshToken (req) {
		try {
			const imgur = await request({
				method: 'POST',
				url: 'https://api.imgur.com/oauth2/token',
				formData: {
					refresh_token: req.user.imgur.refresh_token,
					client_id: _.get(process, 'env.IMGUR_CLIENT_ID'),
					client_secret: _.get(process, 'env.IMGUR_CLIENT_SECRET'),
					grant_type: 'refresh_token',
				},
				json: true,
			});

			_.set(imgur, 'expires', new Date(new Date().getTime() + _.parseInt(imgur.expires_in)));
			delete imgur.expires_in;
			_.set(req, 'user.imgur', imgur);

			await this.User.findOneAndUpdate({ _id: req.user._id }, { $set: { imgur } });
		} catch (err) {
			pino.error('Unable to refresh Imgur token');
			throw err;
		}
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/imgur/list', getUser).use(fileUpload)
			.get('/imgur/list', this.renderImgurList.bind(this));
		router.use('/imgur/upload', getUser).use(fileUpload)
			.get('/imgur/upload', this.renderImgurUpload.bind(this))
			.post('/imgur/upload', this.handleImgurUpload.bind(this));
	}
}

module.exports = new ImgurController();
