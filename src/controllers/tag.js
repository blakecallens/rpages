const _ = require('lodash');
const pino = require('pino')({
	name: 'TagController',
	prettyPrint: true,
});
const getUser = require('../middleware/get-user');
const templateService = require('../services/template');
const postData = require('../data/post');

class TagController {
	/**
	 * Renders the tag page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderTagPage (req, res) {
		try {
			const tag = _.get(req, 'params.tag');
			const posts = await postData.getPosts(req, { hashtags: `#${tag}` }, _.parseInt(_.get(req, 'query.page', 0)));
			res.type('html');
			res.send(templateService.buildTemplate(req, 'tag', {
				posts,
				tag,
				tagPage: true,
				paginationPage: `/tags/${tag}`,
			}));
		} catch (err) {
			pino.error(`Unable to render the tag page for ${_.get(req, 'params.tag')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Builds the routes for the controller
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/tags/:tag', getUser)
			.get('/tags/:tag', this.renderTagPage.bind(this));
	}
}

module.exports = new TagController();
