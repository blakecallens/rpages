const _ = require('lodash');
const pino = require('pino')({
	name: 'UserController',
	prettyPrint: true,
});
const mongoose = require('mongoose');
const consts = require('../consts');
const getUser = require('../middleware/get-user');
const getUserPassive = require('../middleware/get-user-passive');
const templateService = require('../services/template');
const postData = require('../data/post');
const subData = require('../data/sub');
const userData = require('../data/user');
const userSchema = require('../schemas/user');
const subSchema = require('../schemas/sub');

class UserController {
	constructor () {
		this.User = mongoose.model('User', userSchema);
		this.Sub = mongoose.model('Sub', subSchema);
	}

	/**
	 * Renders the user page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderUserPage (req, res) {
		try {
			let user = await userData.getUser(req.params.id);
			if (_.isEmpty(user)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'not-found'));
				return;
			}

			user = user.clean(['customCss', 'spotifyUrl']);
			if (!_.isEmpty(user.spotifyUrl)) {
				if (user.spotifyUrl.match(/soundcloud\.com/)) {
					user.soundCloud = true;
				} else {
					user.spotifyUrl = user.spotifyUrl.replace(/https:\/\/open\.spotify\.com\//, '');
				}
			}
			if (_.get(req, 'user')) {
				user.subscribers = await subData.getUserSubCount(user._id);
			}

			let query = { user: user._id };
			if (_.get(req, 'pageType') && _.get(req, 'user')) {
				switch (req.pageType) {
					case 'history':
						query = {
							$or: [
								{ user: user._id, mentions: `@${req.user.handle}` },
								{ user: req.user._id, mentions: `@${user.handle}` },
							],
						};
						break;
					default:
						break;
				}
			}

			const notLoggedInUser = !user._id.equals(_.get(req, 'user._id'));
			const posts = await postData.getPosts(req, query, _.parseInt(_.get(req, 'query.page', 0)), {
				getReferences: !notLoggedInUser,
				sort: { pinned: -1, created: -1 },
			});
			const subscribed = _.get(req, 'user') && notLoggedInUser ? await subData.userIsSubscribed(req.user._id, user._id) : undefined;
			user.showingEmail = _.get(req, 'user') && notLoggedInUser
				? await subData.userShowingEmail(user._id, req.user._id) : undefined;

			const templateValues = {
				user,
				posts,
				notLoggedInUser,
				subscribed,
				isBlake: user.handle === 'blake',
				returnTo: `${user.handle}${_.get(req, 'pageType') ? `/${req.pageType}` : ''}`,
			};
			_.set(templateValues, _.get(req, 'pageType', 'normal'), true);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'user', templateValues));
		} catch (err) {
			pino.error(`Unable to render the user page for user ${_.get(req, 'params.id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles subscribing to a user
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleSubscribe (req, res) {
		try {
			const subscribed = await userData.getUser(req.params.id);
			if (_.isEmpty(subscribed)) {
				res.redirect(_.get(req, 'query.returnTo', '/dashboard'));
				return;
			}
			if (!(await subData.userIsSubscribed(req.user._id, subscribed._id))) {
				await this.Sub.create({ subscriber: req.user._id, subscribed: subscribed._id });
			}

			res.redirect(_.get(req, 'query.returnTo', `/users/${subscribed.handle}`));
		} catch (err) {
			pino.error(err);
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles unsubscribing from a user
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleUnsubscribe (req, res) {
		try {
			const subscribed = await userData.getUser(req.params.id);
			if (_.isEmpty(subscribed)) {
				res.redirect(_.get(req, 'query.returnTo', '/dashboard'));
				return;
			}
			await this.Sub.deleteOne({ subscriber: req.user._id, subscribed: subscribed._id });
			res.redirect(_.get(req, 'query.returnTo', `/users/${subscribed.handle}`));
		} catch (err) {
			pino.error(err);
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles showing a user your email
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleShowEmail (req, res) {
		try {
			const subscribed = await userData.getUser(req.params.id);
			if (_.isEmpty(subscribed)) {
				res.redirect(_.get(req, 'query.returnTo', '/dashboard'));
				return;
			}

			if (await subData.userIsSubscribed(req.user._id, subscribed._id)) {
				await this.Sub.updateOne(
					{ subscriber: req.user._id, subscribed: subscribed._id },
					{ showEmail: true },
				);
			}

			res.redirect(_.get(req, 'query.returnTo', '/subs'));
		} catch (err) {
			pino.error(err);
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Handles hiding your email from a user
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleHideEmail (req, res) {
		try {
			const subscribed = await userData.getUser(req.params.id);
			if (_.isEmpty(subscribed)) {
				res.redirect(_.get(req, 'query.returnTo', '/dashboard'));
				return;
			}

			if (await subData.userIsSubscribed(req.user._id, subscribed._id)) {
				await this.Sub.updateOne(
					{ subscriber: req.user._id, subscribed: subscribed._id },
					{ showEmail: false },
				);
			}

			res.redirect(_.get(req, 'query.returnTo', '/subs'));
		} catch (err) {
			pino.error(err);
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Renders a single post
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderMentionUser (req, res) {
		try {
			const user = await userData.getUser(req.params.id);
			if (_.isEmpty(user)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'not-found'));
				return;
			}

			res.type('html');
			res.send(templateService.buildTemplate(req, 'posts', {
				user: _.get(req, 'user'),
				loggedIn: true,
				postsPage: true,
				maxChars: consts.MAX_POST_LENGTH,
				content: `@${user.handle}, `,
				returnTo: _.get(req, 'query.returnTo', 'posts'),
			}));
		} catch (err) {
			pino.error(`Unable to render the post details page for user ${_.get(req, 'params.id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Generates a middleware function to set the user page type
	 *
	 * @param {string} type The page type
	 * @returns {Function} The middleware function
	 */
	setUserPageType (type) {
		return (req, res, next) => {
			req.pageType = type;
			next();
		};
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/users/:id', getUserPassive)
			.get('/users/:id', this.renderUserPage.bind(this));
		router.use('/users/:id/history', getUser).use(this.setUserPageType('history'))
			.get('/users/:id/history', this.renderUserPage.bind(this));
		router.use('/users/:id/subscribe', getUser)
			.get('/users/:id/subscribe', this.handleSubscribe.bind(this));
		router.use('/users/:id/unsubscribe', getUser)
			.get('/users/:id/unsubscribe', this.handleUnsubscribe.bind(this));
		router.use('/users/:id/show-email', getUser)
			.get('/users/:id/show-email', this.handleShowEmail.bind(this));
		router.use('/users/:id/hide-email', getUser)
			.get('/users/:id/hide-email', this.handleHideEmail.bind(this));
		router.use('/users/:id/mention', getUser)
			.get('/users/:id/mention', this.renderMentionUser.bind(this));
		router.use('/your-page', getUser)
			.get('/your-page', (req, res) => {
				res.redirect(`/users/${req.user.handle}`);
			});
	}
}

module.exports = new UserController();
