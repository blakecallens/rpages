const _ = require('lodash');
const pino = require('pino')({
	name: 'OAuthController',
	prettyPrint: true,
});
const mongoose = require('mongoose');
const getUser = require('../middleware/get-user');
const userSchema = require('../schemas/user');
const templateService = require('../services/template');


class OAuthController {
	constructor () {
		this.User = mongoose.model('User', userSchema);
	}

	/**
	 * Starts Imgur OAuth
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	handleImgurOAuthStart (req, res) {
		const redirectUrl = `https://api.imgur.com/oauth2/authorize?client_id=${_.get(process, 'env.IMGUR_CLIENT_ID')}&response_type=token&state=IMGUR_OAUTH`;
		res.setHeader('Location', redirectUrl);
		res.sendStatus(302);
	}

	/**
	 * Renders the oauth callback page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	handleImgurOAuthCallback (req, res) {
		res.type('html');
		res.send(templateService.buildTemplate(req, 'imgur-oauth'));
	}

	/**
	 * Handles Imgur oauth finish
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleImgurOAuthFinish (req, res) {
		try {
			const fields = ['access_token', 'expires_in', 'token_type', 'refresh_token', 'account_username', 'account_id'];
			if (_.some(fields, field => _.isEmpty(_.get(req, ['query', field])))) {
				res.redirect('/account/image?imgurError=true');
				return;
			}
			const imgur = _.pick(req.query, fields);
			_.set(imgur, 'expires', new Date(new Date().getTime() + _.parseInt(imgur.expires_in)));
			delete imgur.expires_in;

			await this.User.findOneAndUpdate({ _id: req.user._id }, { $set: { imgur } });
			res.redirect('/account/image');
		} catch (err) {
			pino.error('Unable to handle Imgur oAuth callback');
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'imgur-photos', { error: true }));
		}
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/oauth/imgur/start', getUser)
			.get('/oauth/imgur/start', this.handleImgurOAuthStart.bind(this));
		router.use('/oauth/imgur/callback', getUser)
			.get('/oauth/imgur/callback', this.handleImgurOAuthCallback.bind(this));
		router.use('/oauth/imgur/finish', getUser)
			.get('/oauth/imgur/finish', this.handleImgurOAuthFinish.bind(this));
	}
}

module.exports = new OAuthController();
