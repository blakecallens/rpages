const _ = require('lodash');
const pino = require('pino')({ prettyPrint: true });
const consts = require('../consts');
const getUser = require('../middleware/get-user');
const cookieService = require('../services/cookie');
const redisService = require('../services/redis');
const templateService = require('../services/template');

class SigninController {
	/**
	 * Handles signing out
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async handleSignout (req, res) {
		try {
			cookieService.delete(res, consts.SESSION_COOKIE_NAME);
			await redisService.delete(`${consts.SESSION_PREFIX}${_.get(req, ['cookies', consts.SESSION_COOKIE_NAME])}`);
			res.redirect('/');
		} catch (err) {
			pino.error(err);
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/signout', getUser)
			.get('/signout', this.handleSignout.bind(this));
	}
}

module.exports = new SigninController();
