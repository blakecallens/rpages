const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const pino = require('pino')({
	name: 'AssetsController',
	prettyPrint: true,
});

class AssetsController {
	constructor () {
		this.cache = {};
	}

	/**
	 * Retrieves and returns an asset from the assets directory or memory
	 *
	 * @param {object} req The Express request object
	 * @param {object} res The Express response object
	 */
	getAsset (req, res) {
		try {
			const folder = _.get(req, 'params.folder');
			const fileName = _.get(req, 'params.fileName');
			const extension = _.last(fileName.split('.'));
			res.type(extension);

			if (this.cache[fileName]) {
				res.send(this.cache[fileName]);
				return;
			}

			const filePath = folder
				? path.join(__dirname, '..', '..', 'assets', folder, fileName)
				: path.join(__dirname, '..', '..', 'assets', fileName);
			if (!fs.existsSync(filePath)) {
				res.sendStatus(404);
				return;
			}

			const file = fs.readFileSync(filePath);
			if (!_.intersection(['js', 'css'], [extension]).length) {
				this.cache[fileName] = file;
			}

			res.send(file);
		} catch (err) {
			pino.error(`Unable to retrieve asset ${_.get(req, 'params.fileName')}`);
			pino.error(err);
			res.sendStatus(500);
		}
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.get('/favicon.ico', (req, res) => {
			req.params = { fileName: 'favicon.ico' };
			this.getAsset(req, res);
		});
		router.get('/manifest.webmanifest', (req, res) => {
			req.params = { fileName: 'manifest.webmanifest' };
			this.getAsset(req, res);
		});
		router.get('/service-worker.js', (req, res) => {
			req.params = { fileName: 'service-worker.js' };
			this.getAsset(req, res);
		});
		router.get('/assets/:fileName', this.getAsset.bind(this));
		router.get('/assets/:folder/:fileName', this.getAsset.bind(this));
	}
}

module.exports = new AssetsController();
