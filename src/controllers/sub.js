const _ = require('lodash');
const pino = require('pino')({
	name: 'SubController',
	prettyPrint: true,
});
const getUser = require('../middleware/get-user');
const templateService = require('../services/template');
const subData = require('../data/sub');
const userData = require('../data/user');

class SubController {
	/**
	 * Renders the subs page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderSubsPage (req, res) {
		try {
			const page = _.parseInt(_.get(req, 'query.page', 0));
			const search = _.get(req, 'query.search');
			const subs = await subData.getUserSubscriptions(req.user._id, page, { search });
			_.each(subs.docs, doc => _.assignIn(doc, {
				user: _.assignIn(doc.subscribed, {
					notLoggedInUser: true,
					subscribed: true,
				}),
				returnTo: encodeURIComponent(`/subs?page=${page}`),
			}));

			res.type('html');
			res.send(templateService.buildTemplate(req, 'subs', {
				subs,
				search,
				subsPage: true,
				userSubs: true,
			}));
		} catch (err) {
			pino.error(`Unable to render the subs page for user ${_.get(req, 'user._id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Renders the user subs page
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderUserSubsPage (req, res) {
		try {
			let user = await userData.getUser(req.params.id);
			if (_.isEmpty(user)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'not-found'));
				return;
			}
			user = user.clean(['customCss', 'spotifyUrl']);

			const page = _.parseInt(_.get(req, 'query.page', 0));
			const search = _.get(req, 'query.search');
			const subs = await subData.getUserSubscribers(req, user._id, page, { search });
			_.each(subs.docs, doc => _.assignIn(doc, {
				user: doc.subscriber,
				returnTo: encodeURIComponent(`/subs/${req.params.id}?page=${page}`),
			}));
			user.subscribers = await subData.getUserSubCount(user._id);

			res.type('html');
			res.send(templateService.buildTemplate(req, 'subs', {
				user,
				subs,
				search,
				subsPage: true,
				notLoggedInUser: _.get(req, 'user') ? !req.user._id.equals(user._id) : undefined,
				subscribed: _.get(req, 'user') ? await subData.userIsSubscribed(req.user._id, user._id) : undefined,
			}));
		} catch (err) {
			pino.error(`Unable to render the subscribers page for user ${_.get(req, 'params.id')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/subs', getUser)
			.get('/subs', this.renderSubsPage.bind(this));
		router.use('/subs/:id', getUser)
			.get('/subs/:id', this.renderUserSubsPage.bind(this));
	}
}

module.exports = new SubController();
