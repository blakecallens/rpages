const _ = require('lodash');
const pino = require('pino')({ prettyPrint: true });
const getUser = require('../middleware/get-user');
const templateService = require('../services/template');
const userData = require('../data/user');

class SearchController {
	/**
	 * Handles subscribing to a user
	 *
	 * @param {object} req The express request object
	 * @param {object} res The express response object
	 */
	async renderUserSearchPage (req, res) {
		try {
			const search = _.trim(_.get(req, 'query.search'));
			if (_.isEmpty(search)) {
				res.type('html');
				res.send(templateService.buildTemplate(req, 'search-users', { user: req.user, searchPage: true }));
				return;
			}

			let query;
			if (search.indexOf(' ') > -1) {
				const words = search.split(' ');
				query = {
					firstName: { $regex: new RegExp(`.*${_.head(words)}.*`, 'i') },
					lastName: { $regex: new RegExp(`.*${_.tail(words).join(' ')}.*`, 'i') },
				};
			} else {
				query = {
					$or: [
						{ handle: { $regex: new RegExp(`.*${search}.*`, 'i') } },
						{ firstName: { $regex: new RegExp(`.*${search}.*`, 'i') } },
						{ lastName: { $regex: new RegExp(`.*${search}.*`, 'i') } },
					],
				};
			}

			const users = await userData.getUsers(req, query, _.parseInt(_.get(req, 'query.page', 0)));
			users.docs = _.map(users.docs, user => ({
				user,
				returnTo: encodeURIComponent(`/search/users?search=${search}`),
			}));

			res.type('html');
			res.send(templateService.buildTemplate(req, 'search-users', {
				search,
				users: !_.isEmpty(users) ? users : undefined,
				searchPage: true,
				paginationPage: `/search/users?search=${search}`,
			}));
		} catch (err) {
			pino.error(`Unable to render the user search page for query ${_.get(req, 'query.search')}`);
			pino.error(err);
			res.type('html');
			res.send(templateService.buildTemplate(req, 'error'));
		}
	}

	/**
	 * Builds the routes for the controller
	 *
	 * @param {object} router The Express router object
	 */
	getRoutes (router) {
		router.use('/search/users', getUser)
			.get('/search/users', this.renderUserSearchPage.bind(this));
	}
}

module.exports = new SearchController();
