const _ = require('lodash');

module.exports = {
	CAPTCHA_PREFIX: 'captcha_prefix_',
	DEFAULT_TTL: 15 * 60,
	EMAIL_VERIFICATION_PREFIX: 'email_verify_',
	HOST: _.get(process, 'env.HOST', 'localhost:8080'),
	MAX_ABOUT_LENGTH: 240,
	MAX_CSS_LENGTH: 4000,
	MAX_FILE_SIZE: 6 * 1024 * 1024,
	MAX_IMAGE_WIDTH: 500,
	MAX_POST_LENGTH: 2500,
	MIN_AGE: 18,
	PAGE_LIMIT: 10,
	PASSWORD_RESET_PREFIX: 'password_reset_',
	POST_LIMIT_PREFIX: 'post_limit_',
	SESSION_COOKIE_NAME: 'sessionId',
	SESSION_EXPIRY_HOURS: 72,
	SESSION_PREFIX: 'session_',
	USER_IMAGE_SIZE: 150,
};
