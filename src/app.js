const _ = require('lodash');
const express = require('express');
const compression = require('compression');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const find = require('find');
const mongoose = require('mongoose');
const pino = require('pino')({
	name: 'main',
	prettyPrint: true,
});

const sameDomain = require('./middleware/same-domain');
const cleanHeaders = require('./middleware/clean-headers');
const forceForward = require('./middleware/force-forward');
const forceSSL = require('./middleware/force-ssl');

async function main () {
	try {
		const dbUrl = _.get(process, 'env.MONGODB_URI', 'mongodb://localhost/hermitage');
		await mongoose.connect(dbUrl, {
			useCreateIndex: true,
			useFindAndModify: false,
			useNewUrlParser: true,
			useUnifiedTopology: true,
		});
		pino.debug(`Connected to DB at ${dbUrl}`);

		const app = express();
		const port = process.env.PORT || 8080;
		const router = express.Router();

		app.use(forceForward);
		app.use(sameDomain);
		app.use(cleanHeaders);
		app.use(forceSSL);
		app.use(compression());
		app.use(bodyParser.urlencoded({ extended: true }));
		app.use(cookieParser());
		app.use('', router);

		// Get the routes from controllers
		try {
			find.eachfile(path.join(__dirname, 'controllers'), file => {
				const controller = require(file); // eslint-disable-line
				if (_.get(controller, 'getRoutes')) {
					controller.getRoutes(router);
				}
			});
		} catch (err) {
			pino.error('Error building routes');
			pino.error(err);
			process.exit(1);
		}

		app.listen(port);
		pino.info(`Listening on port ${port}`);
	} catch (err) {
		pino.error(err);
	}
}

main();
