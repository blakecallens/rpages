const _ = require('lodash');
const redis = require('redis');
const client = redis.createClient(_.get(process, 'env.REDIS_URL'));

class RedisService {
	/**
	 * Pings redis
	 */
	async ping () {
		return new Promise((resolve, reject) => {
			client.ping(err => {
				if (err) {
					reject(err);
					return;
				}
				resolve();
			});
		});
	}

	/**
	 * Retrieves a value from redis
	 *
	 * @param {string} key The key to retrieve
	 * @returns {object|string} The redis value
	 */
	async get (key) {
		return new Promise((resolve, reject) => {
			client.get(key, (err, result) => {
				if (err) {
					reject(err);
					return;
				}
				try {
					resolve(JSON.parse(result));
				} catch (e) {
					resolve(result);
				}
			});
		});
	}

	/**
	 * Sets a value in redis
	 *
	 * @param {string} key The key to set
	 * @param {object|string} value The value to store
	 */
	async set (key, value) {
		let storeValue = value;
		if (_.isObject(value)) {
			storeValue = JSON.stringify(value);
		}
		return new Promise((resolve, reject) => {
			client.set(key, storeValue, (err, result) => {
				if (err) {
					reject(err);
					return;
				}
				resolve(result);
			});
		});
	}

	/**
	 * Sets the expiry of a redis key
	 *
	 * @param {string} key The key to set expiry for
	 * @param {number} hours The number of hours to expire in
	 */
	async expire (key, hours) {
		return new Promise((resolve, reject) => {
			client.expire(key, hours * 60 * 60, (err, result) => {
				if (err) {
					reject(err);
					return;
				}
				resolve(result);
			});
		});
	}

	/**
	 * Deletes a value in redis
	 *
	 * @param {string} key The key to delete
	 */
	async delete (key) {
		return new Promise((resolve, reject) => {
			client.del(key, (err, result) => {
				if (err) {
					reject(err);
					return;
				}
				resolve(result);
			});
		});
	}
}

module.exports = new RedisService();
