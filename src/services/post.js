const _ = require('lodash');
const pino = require('pino')({
	name: 'PostService',
	prettyPrint: true,
});
const octicon = require('helper-octicon');
const mongoose = require('mongoose');
const postSchema = require('../schemas/post');
const i18nService = require('./i18n');
const templateService = require('./template');
const dataUtil = require('../data/util');

const HOST = _.get(process, 'env.HOST', 'localhost:8080');
const BITCHUTE_URL_REGEX = /(https?:\/\/)?(www\.)?(bitchute\.com)\/(video|embed)[^(\s)]+/g;
const BITCHUTE_ID_REGEX = /(\/?)(video|embed)(\/?)(\S+)$/gm;
const GIPHY_EMBED_REGEX = /https:\/\/giphy\.com\/embed\//;
const SELF_URL_REGEX = new RegExp(`https?://${HOST}`);
const SELF_POST_REGEX = /(\/?)posts(\/?)(\S+)?/gm;
const SELF_USER_REGEX = /(\/?)users(\/?)(\S+)?/gm;
const SPOTIFY_URL_REGEX = /https:\/\/open\.spotify\.com\//;
const SOUNDCLOUD_URL_REGEX = /https:\/\/soundcloud\.com\//;
const PREVIEW_POST_REGEX = /%\^p.*%\^p/g;
const URL_REGEX = /https?:\/\/[^(\s)]+/g;
const YOUTUBE_ID_REGEX = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w-]+\?v=|embed\/|v\/)?)([\w-]+)(\S+)?$/gm;
const YOUTUBE_URL_REGEX = /(https?:\/\/)?((www|m)\.)?(youtube\.com|youtu\.?be)[^(\s)]+/g;
const CODEPEN_URL_REGEX = /https:\/\/codepen\.io\//;

const BITCHUTE_EMBED = `<a class="text-xsmall" href="{{url}}" target="_blank">{{url}} ${octicon('link-external', { class: 'link' })}</a><div class="video-container"><iframe src="https://www.bitchute.com/embed/{{id}}/" allowfullscreen></iframe></div>`;
const GIPHY_EMBED = '<div class="giphy-embed"><iframe src="{{url}}"></iframe></div>';
const SPOTIFY_EMBED = `<a class="text-xsmall" href="{{url}}" target="_blank">{{url}} ${octicon('link-external', { class: 'link' })}</a><div class="spotify-embed large"><iframe src="https://open.spotify.com/embed/{{id}}" allow="encrypted-media"></iframe></div>`;
const SOUNDCLOUD_EMBED = `<a class="text-xsmall" href="{{url}}" target="_blank">{{url}} ${octicon('link-external', { class: 'link' })}</a><div class="soundcloud-embed large"><iframe scrolling="no" frameborder="0" allowtransparency="true" width="100%" height="300" src="https://w.soundcloud.com/player/?url={{url}}?visual=true"></iframe></div>`;
const YOUTUBE_EMBED = `<a class="text-xsmall" href="{{url}}" target="_blank">{{url}} ${octicon('link-external', { class: 'link' })}</a><br/><div class="video-container"><iframe src="https://www.youtube.com/embed/{{id}}" allowfullscreen></iframe></div>`;
const CODEPEN_EMBED = '<div class="codepen-embed"><iframe scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" src="{{url}}"></iframe></div>';

class PostService {
	constructor () {
		this.Post = mongoose.model('Post', postSchema);
	}

	async replaceUrls (req, content, blockPostEmbed = false) {
		let newContent = content.replace(/\n/g, '<br/>');

		let matches = newContent.match(/@[^(\s|.|,)]+/g);
		newContent = _.reduce(matches, (memo, match) => memo.replace(match, `<a href="/users/${match.substr(1)}">${match}</a>`), newContent);

		matches = newContent.match(/#[^(\s)]+/g);
		newContent = _.reduce(matches, (memo, match) => memo.replace(match, `<a href="/tags/${match.substr(1)}">${match}</a>`), newContent);

		matches = newContent.match(URL_REGEX);
		newContent = _.reduce(matches, (memo, match) => {
			switch (_.last(match.split('.'))) {
				case 'gif':
				case 'png':
				case 'jpg':
					return memo.replace(match, `<div class="text-center"><a href="${match}" target="_blank"><img class="content-image" src="${match}"></a></div>`);
				default:
					return this.replaceUrl(req, memo, match);
			}
		}, newContent);

		matches = newContent.match(PREVIEW_POST_REGEX);
		if (!_.isEmpty(matches)) {
			// eslint-disable-next-line
			for (let match of matches) {
				newContent = newContent.replace(match, '');
				if (blockPostEmbed) {
					break;
				}

				try {
					const _id = match.replace(/%\^p/g, '');
					const post = (await this.Post.findOne({ _id }).populate('user')).toObject();
					if (!_.isEmpty(post)) {
						post.user = _.omit(post.user, 'password');
						post.content = await this.replaceUrls(req, post.content, true);
						post.readableDate = dataUtil.getReadableDate(post.created);
						post.createdISO = post.created.toISOString();
					}

					const reference = templateService.buildTemplate(req, 'post-ref', { post });
					newContent = `${newContent}${reference}`;
				} catch (err) {
					pino.debug(`Error replacing URL ${match}`);
				}
			}
		}

		return newContent;
	}

	/**
	 * Replaces a URL in post content
	 *
	 * @param {object} req The express request object
	 * @param {string} content The post content
	 * @param {string} url The URL to replace
	 * @returns {string} The updated content
	 */
	replaceUrl (req, content, url) {
		// Only blank target on external links
		if (url.match(SELF_URL_REGEX)) {
			return content.replace(url, this.getSelfLink(url));
		}
		if (url.match(GIPHY_EMBED_REGEX)) {
			return content.replace(url, GIPHY_EMBED.replace(/\{\{url\}\}/g, url)
				.replace(/\{\{powered\}\}/, i18nService.getValue(dataUtil.getLanguage(req), 'POWERED_BY')));
		}
		if (url.match(YOUTUBE_URL_REGEX)) {
			return content.replace(url, YOUTUBE_EMBED.replace(/\{\{id\}\}/g, _.nth(YOUTUBE_ID_REGEX.exec(url), 5))
				.replace(/\{\{url\}\}/g, url));
		}
		if (url.match(BITCHUTE_URL_REGEX)) {
			return content.replace(url, BITCHUTE_EMBED.replace(/\{\{id\}\}/g, (_.nth(BITCHUTE_ID_REGEX.exec(url), 4) || '').replace(/\//g, ''))
				.replace(/\{\{url\}\}/g, url));
		}
		if (url.match(SPOTIFY_URL_REGEX)) {
			return content.replace(url, SPOTIFY_EMBED.replace(/\{\{id\}\}/g, url.replace(SPOTIFY_URL_REGEX, ''))
				.replace(/\{\{url\}\}/g, url));
		}
		if (url.match(SOUNDCLOUD_URL_REGEX)) {
			return content.replace(url, SOUNDCLOUD_EMBED.replace(/\{\{url\}\}/g, url));
		}
		if (url.match(CODEPEN_URL_REGEX)) {
			return content.replace(url, CODEPEN_EMBED.replace(/\{\{url\}\}/g, url.replace(/\/pen\//, '/embed/')));
		}
		// All other URLs
		return content.replace(url, `<a href="${url}" target="_blank">${url} ${octicon('link-external', { class: 'link' })}</a>`);
	}

	/**
	 * Returns a formatted internal link
	 *
	 * @param {string} url The self url
	 * @returns {string} The new link
	 */
	getSelfLink (url) {
		if (url.match(SELF_POST_REGEX)) {
			const _id = _.nth(SELF_POST_REGEX.exec(url), 3);
			return `<a href="/posts/${_id}">...${_id.substr(_id.length - 5, 5)} ${octicon('bookmark', { class: 'link' })}</a>%^p${_id}%^p`;
		}
		if (url.match(SELF_USER_REGEX)) {
			const handle = _.nth(SELF_USER_REGEX.exec(url), 3);
			return `<a href="/users/${handle}">@${handle}</a>`;
		}
		return `<a href="${url}">${url}</a>`;
	}
}

module.exports = new PostService();
