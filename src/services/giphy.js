const _ = require('lodash');
const pino = require('pino')({
	name: 'GiphyService',
	prettyPrint: true,
});
const request = require('request-promise-native');
const consts = require('../consts');

class GiphyService {
	async search (query) {
		try {
			const gifs = await request.get(
				`https://api.giphy.com/v1/gifs/search?api_key=${_.get(process, 'env.GIPHY_API_KEY')}&limit=${consts.PAGE_LIMIT}&q=${query}`,
				{ json: true },
			);
			return _.get(gifs, 'data');
		} catch (err) {
			pino.error('Unable to retrieve giphy search');
			throw err;
		}
	}
}

module.exports = new GiphyService();
