const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const find = require('find');
const handlebars = require('handlebars');
const helperOcticon = require('helper-octicon');
const i18nService = require('./i18n');
const consts = require('../consts');

const HOST = _.get(process, 'env.HOST', 'localhost:8080');

class TemplateService {
	constructor () {
		handlebars.registerHelper('octicon', helperOcticon);

		// Set templates and partials
		this.templates = {};
		find.eachfile(path.join(__dirname, '..', 'templates'), file => {
			const templateName = _.last(file.split(path.sep)).replace(/\.hbs/, '');
			if (file.indexOf('partials') > -1) {
				handlebars.registerPartial(templateName, fs.readFileSync(file).toString());
			} else {
				_.set(this.templates, templateName, handlebars.compile(fs.readFileSync(file).toString()));
			}
		});
	}

	/**
	 * Builds a template
	 *
	 * @param {object} req The express request object
	 * @param {string} name The name of the template
	 * @param {object} [values] Values to inject into the template
	 * @returns {string} The built template
	 */
	buildTemplate (req, name, values = {}) {
		if (!_.get(this.templates, name)) {
			return '';
		}

		const language = _.head(req.acceptsLanguages());
		_.assignIn(values, {
			host: HOST,
			maxChars: _.get(values, 'maxChars', consts.MAX_POST_LENGTH),
			returnTo: _.get(values, 'returnTo', _.head(_.get(req, 'url').split('?'))),
			year: new Date().getFullYear(),
			loggedIn: !_.isEmpty(_.get(req, 'user')),
			user: _.get(values, 'user', _.get(req, 'user')),
			postSuccess: _.get(req, 'query.postSuccess'),
			postLimit: _.get(req, 'query.postLimit'),
		});
		_.set(values, 'i18n', i18nService.getLocalization(language, values));

		return this.templates[name](values);
	}
}

module.exports = new TemplateService();
