const _ = require('lodash');
const pino = require('pino')({
	name: 'ImageService',
	prettyPrint: true,
});
const imageSize = require('image-size');
const sharp = require('sharp');
const cloudinary = require('cloudinary');
const streamifier = require('streamifier');
const consts = require('../consts');

class ImageService {
	constructor () {
		cloudinary.config({
			cloud_name: _.get(process, 'env.CLOUDINARY_NAME'),
			api_key: _.get(process, 'env.CLOUDINARY_KEY'),
			api_secret: _.get(process, 'env.CLOUDINARY_SECRET'),
		});
	}

	/**
	 * Gets the size of an image buffer
	 *
	 * @param {Buffer} buffer The image buffer
	 * @returns {object} The image dimensions
	 */
	getImageBufferSize (buffer) {
		return imageSize(buffer);
	}

	/**
	 * Resizes an image and uploads it to cloudinary
	 *
	 * @param {Buffer} buffer The image file buffer
	 * @returns {string} The image URL
	 */
	async resizeAndSaveUserImage (buffer) {
		try {
			const fileBuffer = await sharp(buffer)
				.rotate()
				.resize(consts.USER_IMAGE_SIZE, consts.USER_IMAGE_SIZE)
				.toBuffer();

			const imageUrl = await new Promise((resolve, reject) => {
				const uploadStream = cloudinary.v2.uploader.upload_stream({ folder: 'profile-images' }, (err, result) => {
					if (err) {
						reject(err);
						return;
					}
					resolve(_.get(result, 'url'));
				});

				streamifier.createReadStream(fileBuffer).pipe(uploadStream);
			});

			return imageUrl.replace(/https?:/, '');
		} catch (err) {
			pino.error('Unable to upload image');
			throw err;
		}
	}

	/**
	 * Resizes an image and returns the buffer
	 *
	 * @param {Buffer} buffer The image file buffer
	 * @param {number} width The width to resize to
	 * @returns {Buffer} The resized buffer
	 */
	async resizeImage (buffer, width) {
		try {
			return await sharp(buffer).rotate().resize(width).toBuffer();
		} catch (err) {
			pino.error('Unable to resize image');
			throw err;
		}
	}

	/**
	 * Rotates a user image 90 degrees clockwise
	 *
	 * @param {string} imageUrl The URL of the image
	 */
	rotateImage (imageUrl) {
		let back = imageUrl.replace(/.*\/\/.*upload\//, '');
		const front = imageUrl.replace(back, '');

		let angle = 0;
		if (back.substr(0, 2) === 'a_') {
			angle = _.parseInt(_.head(back.split('/')).replace('a_', ''));
			back = _.tail(back.split('/')).join('/');
		}

		angle += 90;
		if (angle >= 360) {
			return `${front}${back}`;
		}
		return `${front}a_${angle}/${back}`;
	}

	/**
	 * Deletes an image in Cloudinary
	 *
	 * @param {string} imageUrl The URL of the image
	 */
	async removeUserImage (imageUrl) {
		const fileName = _.last(imageUrl.split('/')).replace('.jpg', '');
		try {
			await new Promise((resolve, reject) => {
				cloudinary.v2.uploader.destroy(`profile-images/${fileName}`, err => {
					if (err) {
						reject(err);
						return;
					}
					resolve();
				});
			});
		} catch (err) {
			pino.warn(`Unable to delete image ${imageUrl}`);
			pino.warn(err);
		}
	}
}

module.exports = new ImageService();
