const _ = require('lodash');
const path = require('path');
const find = require('find');

class I18nService {
	constructor () {
		// Set languages
		this.languages = {};
		find.eachfile(path.join(__dirname, '..', 'i18n'), file => {
			const languageName = _.last(file.split(path.sep)).replace(/\.js/, '');
			_.set(this.languages, languageName, require(file)); // eslint-disable-line
		});
	}

	/**
	 * Retrieves the localization object for a language
	 *
	 * @param {string} language The language to use
	 * @param {object} [values] Values to inject into localization
	 * @returns {object} The localization object
	 */
	getLocalization (language, values = {}) {
		const i18n = _.clone(_.get(this.languages, language.toLowerCase(), this.languages['en-us']));
		return _.reduce(i18n, (memo, value, key) => {
			_.set(memo, key, this.insertValues(value, values));
			return memo;
		}, {});
	}

	/**
	 * Retrieves the localization string for a specific key
	 *
	 * @param {string} language The language to use
	 * @param {string} key The key to return
	 * @param {object} [values] Values to inject into localization
	 * @returns {string} The localized string
	 */
	getValue (language, key, values = {}) {
		const i18n = _.clone(_.get(this.languages, language.toLowerCase(), this.languages['en-us']));
		return this.insertValues(_.get(i18n, key, ''), values);
	}

	/**
	 * Injects values into a localization string
	 *
	 * @param {string} str The string to inject values into
	 * @param {object} [values] The values to inject
	 * @returns {string} The injected string
	 */
	insertValues (str, values) {
		return _.reduce(values, (memo, value, key) => {
			let newVal = memo;
			if (newVal.indexOf(`{{${key}}}`) > -1) {
				newVal = newVal.replace(`{{${key}}}`, value);
			}
			return newVal;
		}, str);
	}
}

module.exports = new I18nService();
