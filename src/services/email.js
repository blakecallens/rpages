const pino = require('pino')({
	name: 'EmailService',
	prettyPrint: true,
});
const nodemailer = require('nodemailer');
const i18nService = require('./i18n');
const templateService = require('./template');
const dataUtil = require('../data/util');

class EmailService {
	constructor () {
		this.transporter = nodemailer.createTransport({
			host: process.env.MAIL_HOST,
			port: process.env.SMTP_PORT,
			secure: true,
			auth: {
				user: process.env.MAIL_USERNAME,
				pass: process.env.MAIL_PASSWORD,
			},
		});
	}

	/**
	 * Sends a password reset email
	 *
	 * @param {object} req The express request object
	 * @param {string} email The email address of the recipient
	 * @param {string} token The verification token
	 */
	async sendPasswordReset (req, email, token) {
		try {
			await this.transporter.sendMail({
				from: `"rPAGES Blake" <${process.env.MAIL_USERNAME}>`,
				to: email,
				subject: i18nService.getValue(dataUtil.getLanguage(req), 'RESET_PASSWORD'),
				html: templateService.buildTemplate(req, 'email-reset-password', { token }),
			});
		} catch (err) {
			pino.error('Unable to send password reset email');
			throw err;
		}
	}

	/**
	 * Sends a verification email
	 *
	 * @param {object} req The express request object
	 * @param {string} email The email address of the recipient
	 * @param {string} token The verification token
	 */
	async sendEmailVerification (req, email, token) {
		try {
			await this.transporter.sendMail({
				from: `"rPAGES Blake" <${process.env.MAIL_USERNAME}>`,
				to: email,
				subject: i18nService.getValue(dataUtil.getLanguage(req), 'VERIFY_EMAIL'),
				html: templateService.buildTemplate(req, 'email-verify-email', { token }),
			});
		} catch (err) {
			pino.error('Unable to send verification email');
			throw err;
		}
	}

	/**
	 * Sends a email change verification email
	 *
	 * @param {object} req The express request object
	 * @param {string} email The email address of the recipient
	 * @param {string} token The verification token
	 */
	async sendEmailChange (req, email, token) {
		try {
			await this.transporter.sendMail({
				from: `"rPAGES Blake" <${process.env.MAIL_USERNAME}>`,
				to: email,
				subject: i18nService.getValue(dataUtil.getLanguage(req), 'CHANGE_EMAIL'),
				html: templateService.buildTemplate(req, 'email-change-email', { token }),
			});
		} catch (err) {
			pino.error('Unable to send email change email');
			throw err;
		}
	}
}

module.exports = new EmailService();
