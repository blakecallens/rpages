const consts = require('../consts');

class CookieService {
	/**
	 * Sets a cookie in the response
	 *
	 * @param {object} res The express response object
	 * @param {string} name The name of the cookie
	 * @param {string} value The value of the cookie
	 * @param {number} ttl The time to live of the cookie
	 */
	set (res, name, value, ttl) {
		res.cookie(name, value, { maxAge: ttl || consts.DEFAULT_TTL, httpOnly: true });
	}

	/**
	 * Removes a cookie in the response
	 *
	 * @param {object} res The express response object
	 * @param {string} name The name of the cookie
	 */
	delete (res, name) {
		res.clearCookie(name);
	}
}

module.exports = new CookieService();
