const _ = require('lodash');
const uuid = require('uuid');
const { DateTime } = require('luxon');
const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const bcrypt = require('bcrypt');
const consts = require('../consts');

const SALT_WORK_FACTOR = 10;

const userSchema = new Schema({
	email: {
		type: String,
		required: true,
		unique: true,
		lowercase: true,
		trim: true,
	},
	handle: {
		type: String,
		lowercase: true,
		unique: true,
		default: uuid.v4(),
	},
	password: {
		type: String,
		required: true,
	},
	firstName: {
		type: String,
		required: true,
		trim: true,
	},
	lastName: {
		type: String,
		trim: true,
	},
	dateOfBirth: {
		type: Date,
		required: true,
	},
	about: {
		type: String,
		maxlength: consts.MAX_ABOUT_LENGTH,
	},
	image: {
		type: String,
	},
	website: {
		type: String,
	},
	customCss: {
		type: String,
		maxlength: consts.MAX_CSS_LENGTH,
	},
	spotifyUrl: {
		type: String,
	},
	imgur: {
		access_token: {
			type: String,
		},
		expires: {
			type: Date,
		},
		token_type: {
			type: String,
		},
		refresh_token: {
			type: String,
		},
		account_username: {
			type: String,
		},
		account_id: {
			type: String,
		},
	},
	darkMode: {
		type: Boolean,
		default: false,
	},
	hideNonSubMentions: {
		type: Boolean,
		default: false,
	},
	showFirehose: {
		type: Boolean,
		default: false,
	},
	verified: {
		type: Boolean,
		default: false,
	},
	created: {
		type: Date,
		default: new Date(),
	},
	lastLogin: {
		type: Date,
	},
});

userSchema.plugin(mongoosePaginate);

userSchema.pre('save', function (next) {
	const user = this;

	if (!user.isModified('password')) {
		next();
		return;
	}

	/* eslint-disable prefer-arrow-callback */
	bcrypt.genSalt(SALT_WORK_FACTOR, function (saltErr, salt) {
		if (saltErr) {
			next(saltErr);
			return;
		}

		bcrypt.hash(user.password, salt, function (hashErr, hash) {
			if (hashErr) {
				next(hashErr);
				return;
			}

			user.password = hash;
			next();
		});
	});
});

userSchema.methods.clean = function (extraFields = []) {
	const user = _.pick(this, [
		'_id',
		'firstName',
		'lastName',
		'email',
		'handle',
		'image',
		'about',
		'website',
		'darkMode',
		'hideNonSubMentions',
		'showFirehose',
		'dateOfBirth',
		'created',
	].concat(_.without(extraFields, 'password')));
	if (!_.isEmpty(user.website)) {
		user.displayWebsite = user.website.replace(/https?:\/\//, '');
	}
	if (!_.get(user, 'imgur.access_token')) {
		delete user.imgur;
	}
	user.memberSince = DateTime.fromJSDate(user.created).toLocaleString(DateTime.DATE_SHORT);

	return user;
};

userSchema.methods.comparePassword = async function (candidatePassword) {
	return bcrypt.compare(candidatePassword, this.password);
};

module.exports = userSchema;
