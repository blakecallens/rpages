const _ = require('lodash');
const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const consts = require('../consts');

const postSchema = new Schema({
	user: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	content: {
		type: String,
		required: true,
		maxlength: consts.MAX_POST_LENGTH,
	},
	mentions: [{
		type: String,
	}],
	references: [{
		type: String,
	}],
	hashtags: [{
		type: String,
	}],
	pinned: {
		type: Boolean,
		default: false,
	},
	created: {
		type: Date,
		default: new Date(),
	},
});

postSchema.plugin(mongoosePaginate);

// Remove all HTML from post content
postSchema.pre('save', function (next) {
	const post = this;

	if (!_.isEmpty(post.content)) {
		post.content = _.replace(post.content, /<[^>]*>/g, '');
	}
	next();
});

module.exports = postSchema;
