const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const subSchema = new Schema({
	subscriber: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	subscribed: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	showEmail: {
		type: Boolean,
		default: false,
	},
	created: {
		type: Date,
		default: new Date(),
	},
});

subSchema.plugin(mongoosePaginate);

module.exports = subSchema;
